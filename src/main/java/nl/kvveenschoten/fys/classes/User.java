package nl.kvveenschoten.fys.classes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import nl.kvveenschoten.fys.Controllers.MainController;

/**
 * this class will contain all information about users
 * @author Kevin van veenschoten
 */
public class User {
    
    private final String NAME;
    private final String PASSWORD;
    private String type = "";
    private String Loginlocation = "";
    private String language = "";
    private String lastLoginSucces;
    private String lastLoginAttempt;
    
    public User(String name, String password){
        this.NAME = name;
        this.PASSWORD = password;
    }

    @Override
    public String toString() {
        return String.format("%s %s %s", NAME, PASSWORD, type);
    }
    
    public String getName() {
        return this.NAME;
    }
    
    public String getPassword() {
        return this.PASSWORD;
    }

    public String getType() {
        return type;
    }

    public String getLoginlocation() {
        return Loginlocation;
    }

    public String getLanguage() {
        return language;
    }

    public String getLastLoginSucces() {
        return lastLoginSucces;
    }

    public String getLastLoginAttempt() {
        return lastLoginAttempt;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setLoginlocation(String Loginlocation) {
        this.Loginlocation = Loginlocation;
    }

    public void setLastLoginSucces(String lastLoginSucces) {
        this.lastLoginSucces = lastLoginSucces;
    }

    public void setLastLoginAttempt(String lastLoginAttempt) {
        this.lastLoginAttempt = lastLoginAttempt;
    }
    
    public void setLanguage(String language) {
        this.language = language;
    }
    
    /**
     * Checks if the user is admin
     * @return returns 0 if the user is admin
     */
    public boolean isAdmin(){
        return type.equalsIgnoreCase("admin");
    }

    /**
     * creates a new user
     * @param name new username
     * @param password new password
     * @return return true when no error
     */
    public boolean createUser(String name, String password) {        
        MainController.db.execute("INSERT INTO user (username,password) VALUES ('%v', '%v')", name,password);                
        return true;
    }
    
    /**
     * updates an existing user
     * @param name updated username
     * @param password updated password
     * @param language updated language
     * @param type updated type
     * @return returns true on success
     */
    public boolean updateOtherUser(String name, String password, String language, String type) {
        MainController.db.execute(
            "UPDATE user SET username = '%v', password= '%v', language = '%v', type = '%v' WHERE username = '%v'",
            name,
            password,
            language,
            type,
            name
        );
        return true;
    }
    
    /**
     * updates current users to db
     */
    public void updateCurrentUser() {
        MainController.db.execute(
            "UPDATE user SET username = '%v', password = '%v', language = '%v' WHERE username = '%v'",
            NAME,
            PASSWORD,
            language,
            NAME
        );
    }

    /**
     * selects a user from the db
     * @param username selects this user from db
     * @return return user on success otherwise NULL
     * @throws SQLException 
     */
    public User getUserFromDb(String username) throws SQLException {
        
        Statement sth = MainController.db.execute("SELECT * FROM user WHERE username = '%v'", username);
        ResultSet rs = sth.getResultSet();
        
        if(rs.next()){
            User user = new User(rs.getString("username"), rs.getString("password"));
            user.setLanguage(rs.getString("language"));
            user.setLastLoginAttempt(rs.getString("last_login_attempt"));
            user.setLastLoginSucces(rs.getString("last_login"));            
            user.setLoginlocation(rs.getString("last_login_location"));
            user.setType(rs.getString("type"));
            return user;
        }else{
            return null;
        }                
        
    }

    /**
     * deletes user from db
     * @param name deletes this user
     * @return returns true on succes
     */
    public boolean deleteUserFromDb(String name) {
        MainController.db.execute("DELETE FROM user WHERE username = '%v'",name);
        return true;
    }
}
