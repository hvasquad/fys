package nl.kvveenschoten.fys.classes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.ObservableList;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Daan Oolbekkink
 */
public class ExcelReaderModule {

    private List<ObservableListExtension> sheetsList;
    private XSSFWorkbook workbook;
    private int max;
    private FileInputStream file;
    private List<String> excelRow;
    private String filepath;
    private XSSFSheet sheet;
    private int numberOfSheets;
    private String airportTemp;

    public ExcelReaderModule(String filepath) throws FileNotFoundException, IOException {

        file = new FileInputStream(new File(filepath));
        workbook = new XSSFWorkbook(file);
        numberOfSheets = workbook.getNumberOfSheets();
        this.sheetsList = new ArrayList<>();
        excelRow = new ArrayList<>();
        this.filepath = filepath;
        max = 14;
    }

    public void setFilePath(String filepath) throws FileNotFoundException {
        file = new FileInputStream(new File(filepath));
    }

    public void clearAll() {
        sheetsList.clear();
    }

    public int getNumberOfSheets() {
        return numberOfSheets;
    }

    public ObservableList<FoundLuggage> getLuggageList(int sheetNumber) {
        return sheetsList.get(sheetNumber).getFoundLuggageList();
    }

    public void sendToDatabase(Database db, String tableName) throws SQLException, ClassNotFoundException, ParseException {
        for (int i = 0; i < sheetsList.size(); i++) {
            for (int j = 0; j < sheetsList.get(i).getFoundLuggageList().size(); j++) {
                sheetsList.get(i).getFoundLuggageList().get(j).sendToDatabase(db, tableName);
            }
        }
    }

    public void readExcel() throws IOException {
        for (int i = 0; i < numberOfSheets; i++) {

            //Get first sheet from the workbook
            sheet = workbook.getSheetAt(i);

            sheetsList.add(new ObservableListExtension());

            readSheet(sheet, i);

        }
        file.close();
        FileOutputStream out = new FileOutputStream(new File(filepath));
        workbook.write(out);
        out.close();

    }

    public void readSheet(XSSFSheet sheet, int i) {
        int maxRow = sheet.getLastRowNum();
        int firstRow = sheet.getFirstRowNum();
        FoundLuggage defaultRow;

        for (int rn = firstRow; rn < maxRow; rn++) {
            defaultRow = new FoundLuggage();
            readCellsOfRow(sheet.getRow(rn), defaultRow);
            defaultRow.setAirport(airportTemp);
            if (Character.isDigit(defaultRow.getRegistrationNumber().charAt(0))) {
                sheetsList.get(i).addToList(defaultRow);
            }
        }
    }

    public void readCellsOfRow(XSSFRow row, FoundLuggage defaultRow) {
        //System.out.println("in if");

        for (int cn = 0; cn < 14; cn++) {
            //System.out.println(cn);
            Cell cell = row.getCell(cn, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);
            //System.out.println("in for");
            if (cell == null && defaultRow.getRegistrationNumber().equalsIgnoreCase("x")) {
                break;
            }

            if (cell == null) {
                defaultRow.translateIntToSetter(cn, "x");
                //System.out.println("null if");
                //doe nothing because the defaultRow List has an x for that cell instead.
                
            }else if (airport(cell) && cell.getCellTypeEnum() == CellType.STRING) {
                airportTemp = cell.toString();
                System.out.println(cell.toString());
            }else if (cell.getCellTypeEnum() == CellType.NUMERIC) {
                String str = NumberToTextConverter.toText(cell.getNumericCellValue());
                if (DateUtil.isCellDateFormatted(cell)) {
                    str = cell.toString();

                    // if the cell contained a time value, the apache library will not properly convert to string
                    if (str.equalsIgnoreCase("31-Dec-1899")) {

                        // compute time in the day in seconds
                        int secsInDay = (int) ((cell.getDateCellValue().getTime() / 1000) % 86400);
                        if (secsInDay < 0) {
                            secsInDay += 86400;
                        }
                        // compute hours, minutes and format the string
                        int hours = secsInDay / 3600 + 1;
                        int minutes = (secsInDay % 3600) / 60;
                        str = String.format("%02d:%02d", hours, minutes);
                    }
                }
                defaultRow.translateIntToSetter(cn, str);

                //System.out.println("uit de switch");
            } else {
                if (cell.toString().contains("'")) {
                    String x = cell.toString();
                    x = x.substring(0, x.indexOf("'")) + "'" + x.substring(x.indexOf("'"), x.length());
                    defaultRow.translateIntToSetter(cn, x);
                } else {
                    defaultRow.translateIntToSetter(cn, cell.toString());
                }
            }
            //System.out.println("uit de if");

        }
        //System.out.println("uit de for");
        //} //else {
        //    System.out.println("niet 14");
        //}
        //System.out.println("uit if2");
    }
    
    public Boolean airport(Cell cell) {
        switch (cell.toString().toUpperCase()) {
                case "AMS":
                    return true;
                case "AYT":
                    return true;
                case "IST":
                    return true;
                case "BJV":
                    return true;
                case "DLM":
                    return true;
                case "ADB":
                    return true;
                case "GZP":
                    return true;
                case "ECN":
                    return true;
                case "RAK":
                    return true;
                case "HER":
                    return true;
                case "KGS":
                    return true;
                case "RHO":
                    return true;
                case "ZTH":
                    return true;
                case "CFU":
                    return true;
                case "MJT":
                    return true;
                case "OHD":
                    return true;
                case "SMI":
                    return true;
                case "LPA":
                    return true;
                case "TFO":
                    return true;
                case "PMI":
                    return true;
                case "AGP":
                    return true;
                case "FUE":
                    return true;
                case "FAO":
                    return true;
                case "ACE":
                    return true;
                case "HRG":
                    return true;
                case "NBE":
                    return true;
                case "DXB":
                    return true;
                case "BOJ":
                    return true;
                case "BJL":
                    return true;
                case "CTA":
                    return true;
            }
        return false;
    }
}
