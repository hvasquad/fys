package nl.kvveenschoten.fys.classes;

import java.sql.ResultSet;
import java.sql.SQLException;
import nl.kvveenschoten.fys.Controllers.MainController;

/**
 *  this file will handle all the login needs
 * @author Kevin van veenschoten
 */
public class Login {
    
    private final String loginNameInput;
    private final String passwordInput;    
    private final Database db;
    private User user;
    private Statement sth;
    private final String FT = MainController.createTime();

    public Login(String loginNameInput, String passwordInput) throws SQLException, ClassNotFoundException {        
        this.loginNameInput = loginNameInput;
        this.passwordInput = passwordInput;
        this.db = MainController.db;        
    }
    
    public User getUser() {
        return user;
    }    
    
    /**
     * 
     * @return if login is success returns 1 otherwise 0     
     * @throws java.sql.SQLException     
     */
    public int login() throws SQLException{
        
        ResultSet rs = requestLoginCredentials();        
        return checkLoginCredentials(rs);
        
    }
    
   /**
    * 
    * @return if username exists returns user record from db
    * @throws SQLException 
    */
    public ResultSet requestLoginCredentials() throws SQLException{
        
        sth = db.execute("SELECT * FROM user WHERE username = '%username'", loginNameInput);
        return sth.getResultSet();
        
    }
    
    /**
     * 
     * @param rs is the information of a user record
     * @return if db password = login password return 1 otherwise 0
     * @throws SQLException 
     */
    public int checkLoginCredentials(ResultSet rs) throws SQLException{
        
        if(rs.next()){
            
            user = new User(rs.getString("username"), rs.getString("password"));
            updateLastLoginAttempt();          
             
            if(user.getName().equals(loginNameInput) && user.getPassword().equals(passwordInput)) {                    
                updateLastLogin();                
                user = user.getUserFromDb(user.getName());
                return 1;
            }
            
        }
        return 0;
    }
    
    /**
     * updates last login attempt
     * @throws SQLException 
     */ 
    public void updateLastLoginAttempt() throws SQLException{
        sth = db.execute("UPDATE user SET last_login_attempt = '%value1'" +
                            "WHERE username='%value2'", FT, user.getName());
    }
    
    /**
     * updates last login and last login location
     * @throws SQLException 
     */
    public void updateLastLogin() throws SQLException{
        sth = db.execute("UPDATE user SET last_login = '%value1', last_login_location = '%v1'" +
                        "WHERE username='%v2'", FT, "NL", user.getName());
    }
    
}
