package nl.kvveenschoten.fys.classes;

/**
 * Internationalization class.
 * 
 * @author Joey Blankendaal (500778751)
 */
public class Internationalization {
    public static final String CONSOLE_PREFIX = "[Internationalization] ";
    
    private static final String FALLBACK_DEFAULT = "en";
    
    private String defaultLanguage;
    private String[] languages;
    
    /**
     * Internationalization() constructor method.
     * 
     * @param languages 
     */
    public Internationalization(String... languages) {
        this.languages = languages;
    }
    
    /**
     * getDefault() method returns the default language.
     * 
     * @return 
     */
    public String getDefault() {
        return this.defaultLanguage;
    }
    
    /**
     * setDefault() method sets the default language that is chosen for when the app starts.
     * 
     * @param language 
     */
    public void setDefault(String language) {
        if(ArrayUtils.containsString(this.languages, language)) {
            this.defaultLanguage = language;
        } else {
            this.defaultLanguage = FALLBACK_DEFAULT;
            
            System.out.println(CONSOLE_PREFIX + "The selected default language couldn't be found in the language array, choosing the fallback default: " + FALLBACK_DEFAULT);
        }
    }
}