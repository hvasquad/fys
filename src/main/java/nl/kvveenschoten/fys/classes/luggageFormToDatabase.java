/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.kvveenschoten.fys.classes;

import java.text.SimpleDateFormat;
import java.util.Date;
import nl.kvveenschoten.fys.Controllers.MainController;

/**
 *
 * @author Keegan (500781475)
 */

public class luggageFormToDatabase {
    // final because these variables cannot change after being passed from the controller!
    private final String name;
    private final String firstLetter;
    private final String surname;
    private final String flightnumber;
    private final String cellphone;
    private final String email;
    private final String labelnumber;
    private final String color1;
    private final String color2;
    private final String size;
    private final String location;
    private final String weight;
    private final String brand;
    private final String properties;
    private final String createdDate;
    private final String createdTime;
    
    // TODO
    // 1: Ask how registrationNumber is generated
    // 2: Add type to the database
    private String registrationNumber;
    private String type = "GucciSwekkerBag";

    // This function creates the local date as a String
    public String createDate() {
        Date dNow = new Date();
        SimpleDateFormat temp = new SimpleDateFormat("yyyy-MM-dd'T'HH.mm.ss");
        return temp.format(dNow);
    }
    
    // This function creates the local time as a String
    public String createTime(){
        Date dNow = new Date();
        SimpleDateFormat localTime = new SimpleDateFormat("HH:mm:ss");
        return localTime.format(dNow);
    }
    
    // This function returns the firstletter of the name
    public String firstLetterName(){
        String firstL = String.valueOf(name.charAt(0));
        String firstLetter = firstL.toUpperCase();
        return firstLetter;
    }

    // Here do we need an constructor who is link the variables etc...    
    public luggageFormToDatabase(String name, String surname, String flightnumber,
            String cellphone, String email, String labelnumber, String chosenColor,
            String chosenColor2, String size, String location, String weight,
            String brand, String properties) {
        System.out.println("The beginning of contructor which gives the variables to the PDFPrinting class");
        this.name = name;
        this.firstLetter = firstLetterName();
        this.surname = surname;
        this.flightnumber = flightnumber;
        this.cellphone = cellphone;
        this.email = email;
        this.labelnumber = labelnumber;
        this.color1 = chosenColor;
        this.color2 = chosenColor2;
        this.size = size;
        this.location = location;
        this.weight = weight;
        this.brand = brand;
        this.properties = properties;
        this.createdDate = createDate();
        this.createdTime = createTime();
        
        this.registrationNumber = createDate() + flightnumber;
        System.out.println("End of the constructor every variable has been passed succesfully!");
    } 
    
    // TODO:
    // We need to add some row to the table
    public void insertDatabase(){
        // ("INSERT INTO luggage VALUES (%registration_number, %date_found)", 1234, "2010-03-14");
        MainController.db.execute(
                "INSERT INTO luggage_missing VALUES"
                + " ('%registration_number','%date_found', '%time_found',"
                + "'%type', '%brand', '%arrived_with_flight', '%tag', '%location_found',"
                + "'%main_color', '%second_color', '%size', '%weight', '%passanger_name_and_city',"
                + "'%other_characteristics', 'Amsterdam')",
                registrationNumber, "ff iets anders", createdTime, type, brand, location,
                labelnumber,location, color1, color2, size, weight, firstLetter + ". " + surname,
                properties);
    }
}
