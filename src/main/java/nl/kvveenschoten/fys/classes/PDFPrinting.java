/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.kvveenschoten.fys.classes;

/*
    @author Keegan Meijer (500781475)
*/

import java.awt.Color;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

public class PDFPrinting {
    // Here will... there variables be located
    
    // final because these variables cannot change after being passed from the controller!
    private final String name;
    private final String surname;
    private final String flightnumber;
    private final String cellphone;
    private final String email;
    private final String labelnumber;
    private final String color1;
    private final String color2;
    private final String size;
    private final String location;
    private final String weight;
    private final String brand;
    private final String properties;
    private PDDocument document;
    private PDDocumentInformation pdd;
    private PDPage[] pages;
    private PDPageContentStream contentStream;
    private final String dateTimeCreation;
    
    // You don't need an intialize function. 
    public String createTime() {
        Date dNow = new Date();
        SimpleDateFormat temp = new SimpleDateFormat("yyyy-MM-dd'T'HH.mm.ss");
        return temp.format(dNow);
    }

    // Here do we need an constructor who is link the variables etc...    
    public PDFPrinting(String name, String surname, String flightnumber,
            String cellphone, String email, String labelnumber, String chosenColor,
            String chosenColor2, String size, String location, String weight,
            String brand, String properties) {
        this.pages = new PDPage[1];
        System.out.println("The beginning of contructor which gives the variables to the PDFPrinting class");
        this.name = name;
        this.surname = surname;
        this.flightnumber = flightnumber;
        this.cellphone = cellphone;
        this.email = email;
        this.labelnumber = labelnumber;
        this.color1 = chosenColor;
        this.color2 = chosenColor2;
        this.size = size;
        this.location = location;
        this.weight = weight;
        this.brand = brand;
        this.properties = properties;
        this.dateTimeCreation = createTime();
        System.out.println("End of the constructor every variable has been passed succesfully!");
    }
 
    // Here will the pdf print function be located
    public void create() throws IOException{
        //PDF A4 Dimensions Mac OS 13' 612 × 792, Windows Daan 595x842
        //Create PDF-document object
        document = new PDDocument();
        
        // Remember: It's important to add a page first otherwise the document will be empty
        // It is important aswell because without a page you can't fill the doucemnt!
       addProperties();
       fill();
       save();
    }  
    
    public void addProperties() {
        // Beginning of the created document properties
        //Instantiating the PDDocumentInformation class
        pdd = document.getDocumentInformation();
        //Setting the author of the document
        pdd.setAuthor("Corendon Service Desk");
        // Setting the title of the document
        pdd.setTitle("Vermissingsformulier");
        //Setting the creator of the document
        pdd.setCreator("Corendon");
        //Setting the subject of the document -> dit is de beschrijving!   
        pdd.setSubject("Lost and Found Form");
        //Setting keywords for the document
        pdd.setKeywords("corendon, lost and found, vermissingsformulier, lugage, airplane");
        // Ending of the created document properties

    }
    
    public void fill() throws IOException {
        //Creates the empty page object
        pages[0] = new PDPage();
        //Adds the created empty page object to the PDF-document
        document.addPage(pages[0]);
        
        // Start the contentstream which is needded to "fill the page"
        contentStream = new PDPageContentStream(document, pages[0]);
        drawHeader();
        drawMainContent();   
        contentStream.close(); // do this before saving! 
        // Don't forget to close this stream before saving!
    }
    
    public void save() throws IOException {
        // Save the created PDF-Document
//        document.save("src/main/resources/pdf/" + dateTimeCreation + "_corendon_vermissings_formulier.pdf");
        document.save("pdf/" + dateTimeCreation + "_corendon_vermissings_formulier.pdf");
        System.out.println("PDF DOCUMENT CREATED!");

        //Close document
        document.close();
    }
    
    private void drawHeader() throws IOException{
    // Beginning of the image
        // Creates the image object
        PDImageXObject pdImage = PDImageXObject.createFromFile("src/main/resources/img/CorendonLogo.png", document);
        // De image wordt geadd beginnend vanaf links-onderin met de x en y (5, 706) 
        // De img contrains zijn height en width (75, 112)
        // 612 × 792
        contentStream.drawImage(pdImage, 467, 702, 130, 75);
    // Ending of the image

    // This sets the color of the 'headers horizontal rule' to red
        contentStream.setNonStrokingColor(204, 0, 0);
        // This creates the horizontal rule / rectangle (it's rectangle with small height)
        contentStream.addRect(0, 690, 620, 5);
        // Alles wat boven fill aangegeven staat wordt toegevoegd aan de rectangle
        contentStream.fill();
    // End of the 'headers horizontal rule' 
          
    // Beginning of the titel tekst
        // Text needs to be openend and closed
        contentStream.beginText();
        // Omdat de tekst automatisch gebruik maakt van de nonstrokingcolor
        // moet je de kleur van de tekst weer op black zetten
        // bij de image is hij op red gezet!
        contentStream.setNonStrokingColor(Color.BLACK);
        // This sets the font
        contentStream.setFont(PDType1Font.COURIER, 24);
        // 180 vanaf links en 730 hoog (posities)
        contentStream.newLineAtOffset(40, 730);
        // TODO -> GOOGLE
        contentStream.setLeading(14.5f);
        // Show text
        contentStream.showText("Corendon Vermissingsformulier");
        // text sluiten
        contentStream.endText();
    // Ending of the title text
    }
    
    private void drawMainContent() throws IOException{
        //Begin the Content stream
        contentStream.beginText();
        //Setting the position for the line
        contentStream.newLineAtOffset(40, 650);
        //Setting the font to the Content stream
        contentStream.setFont(PDType1Font.TIMES_ROMAN, 12);
        // Sets the leading of the text
        contentStream.setLeading(14.5f);

        //Adding text in the form of string
        // This is the lefthand side of the lugage form
        // THIS IS THE TITLE
        contentStream.setFont(PDType1Font.TIMES_ROMAN, 18);
        contentStream.newLine();
        contentStream.showText("Persoonsgegevens");
        contentStream.newLine();
        contentStream.newLine();
        contentStream.newLine();
        // THIS IS THE DATA
        contentStream.setFont(PDType1Font.TIMES_ROMAN, 12);
        contentStream.showText("Voornaam: ");
        contentStream.newLine();
        contentStream.newLine();
        contentStream.showText("Achternaam: ");
        contentStream.newLine();
        contentStream.newLine();
        contentStream.showText("Vluchtnummer: ");
        contentStream.newLine();
        contentStream.newLine();
        contentStream.showText("Telefoonnummer: ");
        contentStream.newLine();
        contentStream.newLine();
        contentStream.showText("Email: ");
        contentStream.newLine();
        contentStream.newLine();
        contentStream.endText();
        
        // todo split lugage info with perosnal info
        drawHorizontalRule();
        contentStream.beginText();

        // THIS IS THE DATA
        contentStream.newLineAtOffset(40, 375);
        // This is the data
        contentStream.setFont(PDType1Font.TIMES_ROMAN, 18);
        contentStream.showText("Bagage informatie");
        contentStream.newLine();
        contentStream.newLine();
        contentStream.newLine();
        contentStream.setFont(PDType1Font.TIMES_ROMAN, 12);
        contentStream.showText("Labelnummer: ");
        contentStream.newLine();
        contentStream.newLine();
        contentStream.showText("Kleur: ");
        contentStream.newLine();
        contentStream.newLine();
        contentStream.showText("Formaat: ");
        contentStream.newLine();
        contentStream.newLine();
        contentStream.showText("Locatie verloren: ");
        contentStream.newLine();
        contentStream.newLine();
        contentStream.showText("Gewicht: ");
        contentStream.newLine();
        contentStream.newLine();
        contentStream.showText("Merk: ");
        contentStream.newLine();
        contentStream.newLine();
        contentStream.showText("Kenmerken: ");
        contentStream.newLine();
        contentStream.newLine();
        contentStream.endText();
        
        // This is the right handside of the lugage form
        contentStream.beginText();
        contentStream.newLineAtOffset(150, 650);
        contentStream.setLeading(14.5f);
        contentStream.newLine();
        contentStream.setFont(PDType1Font.COURIER, 12);
        contentStream.newLine();
        contentStream.newLine();
        contentStream.newLine();
        contentStream.showText(this.name);
        contentStream.newLine();
        contentStream.newLine();
        contentStream.showText(this.surname);
        contentStream.newLine();
        contentStream.newLine();
        contentStream.showText(this.flightnumber);
        contentStream.newLine();
        contentStream.newLine();
        contentStream.showText(this.cellphone);
        contentStream.newLine();
        contentStream.newLine();
        contentStream.showText(this.email);
        contentStream.newLine();
        contentStream.newLine();
        contentStream.endText();
        
        // todo split lugage info with perosnal info
        drawHorizontalRule();
        contentStream.beginText();
        contentStream.newLineAtOffset(150, 375);
        contentStream.newLine();
        contentStream.newLine();
        contentStream.newLine();
        contentStream.showText(this.labelnumber);
        contentStream.newLine();
        contentStream.newLine();      
        contentStream.showText(this.color1);
        contentStream.showText("  " + this.color2);
        contentStream.newLine();
        contentStream.newLine();
        contentStream.showText(this.size);
        contentStream.newLine();
        contentStream.newLine();
        contentStream.showText(this.location);
        contentStream.newLine();
        contentStream.newLine();
        contentStream.showText(this.weight);
        contentStream.newLine();
        contentStream.newLine();
        contentStream.showText(this.brand);
        contentStream.newLine();
        contentStream.newLine();
//        if (properties.length()%20==0)
//            String spit = properties.split;
//        else{   
//      Iets van stringsplit op positie 20 etc.. miss?
        contentStream.showText(this.properties);  
        //Ending the content stream
        contentStream.endText();
    }
    
    private void drawHorizontalRule() throws IOException{
        contentStream.setNonStrokingColor(Color.BLACK);
        // This creates the horizontal rule / rectangle (it's rectangle with small height)
        contentStream.addRect(0, 420, 620, 1);
        // Alles wat boven fill aangegeven staat wordt toegevoegd aan de rectangle
        contentStream.fill();
    }

    public String getDateTimeCreation() {
        return dateTimeCreation;
    }
}


/**
 * HERE WILL ALL THE BULLSHIT COMMENTS BE LOCATED
 */
// TODO {add image location with resources from application}
// Create image and add the right location
// Ik moet het pad wss beginnen vanaf img omdat dat de target file aanmaakt tijdens de build
// hij zoekt vanaf de classes folder
// of /classes/img... of / img
//        PDImageXObject pdImage = PDImageXObject.createFromFile("src/main/resources/img/Corendon-Logo.jpg", document);
// ContentStream fills the created page and adds this to the PDF-document
//        PDPageContentStream contentStream = new PDPageContentStream(document, page1);
/*
        here will the contentstream fill logic be displayed
        and here do I have to fill the created pdf
        which ofc wont work with this fkn maven error
        I fkn hate maven I will kill maven I swear 2 god
        ......
        !!!!
        *F!@#$!@$!@
 */

// First we need to load the created document
// Use 'Javas File class' to read the file
//File file = new File("pdf/corendon_vermissings_formulier.pdf");
//        // Loading the file with the method of the PDDocument class (import!)
//        PDDocument doc = document.load(file);

//    // Basic constructor we won't use this one but just for the hell of it
//    public PDFPrinting() {
//        this.pages = new PDPage[1];
//    }