/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.kvveenschoten.fys.classes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Daan Oolbekkink
 */
public class LuggageComparator {
    Database db;
    FoundLuggage fn;

    public LuggageComparator(Database db) {
        this.db = db;
    }
    
    public void CompareLuggageTag() throws SQLException, ClassNotFoundException, ParseException {
        Statement sth = db.execute("SELECT * FROM luggage INNER JOIN luggage_missing ON luggage.tag = luggage_missing.tag");
        ResultSet rs = sth.getResultSet();
        
        while (rs.next()) {
            fn = new FoundLuggage(rs.getString(1), timeStampToStringDate(rs.getString(2)), rs.getString(3),
                    rs.getString(4), rs.getString(5), rs.getString(6),
                    rs.getString(7), rs.getString(8), rs.getString(9),
                    rs.getString(10), rs.getString(11), rs.getString(12),
                    rs.getString(13), rs.getString(14), rs.getString(15));
            fn.sendToDatabaseToSend(db, "luggage_match");
            fn.deleteFromDatabase(db, "luggage");
        }
    }
    
    
    public String timeStampToStringDate(String date) throws SQLException {
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        long i = Long.parseLong(date);
        i = i * 1000;
        //long to date
        Date d = new Date(i);
        return format.format(d);
    }
    
    
    
    
    
}
