package nl.kvveenschoten.fys.classes;

import java.net.URL;
import java.net.URLClassLoader;
import java.sql.SQLException;
import java.util.Locale;
import java.util.ResourceBundle;
import nl.kvveenschoten.fys.MainApp;
import static nl.kvveenschoten.fys.MainApp.internat;

/**
 * a session will be created when a user succesfully logs into the application 
 * @author Kevin van veenschoten
 */
public class SessionModule {
    
    private int sessionState = 0; // 1 active // 0 not active
    private User user; // holds user information    
    private final Login login; 

    /**
     * tries to login to an user account
     *      when success sessionState is 1
     *      when fails sessionState is 0
     * 
     * @param loginInput is the username of the login
     * @param passwordInput is the password of the login          
     */
    public SessionModule(String loginInput, String passwordInput) throws SQLException, ClassNotFoundException {
        
        login = new Login(loginInput, passwordInput);
        
        if(login.login() == 1){
            sessionState = 1;
            user = login.getUser(); 
            setLanguage();
        }                
    }
    
    /**
     * 
     * @return tells if the sessions is active or not
     */
    public int getSessionState() {
        return sessionState;
    }

    /**
     * 
     * @return returns the information of the user thats logged in 
     */
    public User getUser() {
        return user;
    } 
    
    public void setLanguage(){
        String lan = user.getLanguage();
        if(lan.equalsIgnoreCase("nl")){
            URL url = getClass().getResource("/languages/");
            ClassLoader classLoader = new URLClassLoader(new URL[]{url});
            MainApp.language = ResourceBundle.getBundle("messages_nl", Locale.getDefault(), classLoader);
        }else if(lan.equalsIgnoreCase("en")){
            URL url = getClass().getResource("/languages/");
            ClassLoader classLoader = new URLClassLoader(new URL[]{url});
            MainApp.language = ResourceBundle.getBundle("messages_en", Locale.getDefault(), classLoader);
        }else{
            URL url = getClass().getResource("/languages/");
            ClassLoader classLoader = new URLClassLoader(new URL[]{url});
            MainApp.language = ResourceBundle.getBundle("messages" + MainApp.internat.getDefault(), Locale.getDefault(), classLoader);
        }
    }
    
}
