/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.kvveenschoten.fys.classes;

import java.sql.SQLException;
import java.text.ParseException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Daan Oolbekkink
 */
public class ObservableListExtension {
    private final ObservableList<FoundLuggage> foundLuggageList;

    public ObservableListExtension() {
        foundLuggageList = FXCollections.observableArrayList();
    }
    
    public void addToList(FoundLuggage list){
        foundLuggageList.add(list);
    }
    
    public ObservableList<FoundLuggage> getFoundLuggageList() {
        return foundLuggageList;
    }
    
    public void sendToDatabase(Database db, String tableName) throws SQLException, ClassNotFoundException, ParseException {
        for (int i = 0; i < foundLuggageList.size(); i++) {
            foundLuggageList.get(i).sendToDatabase(db, tableName);
            
        }
    }
}
