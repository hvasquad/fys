/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.kvveenschoten.fys.classes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Daan Oolbekkink
 */
public class FoundLuggage {

    private final StringProperty airport;
    private final StringProperty registrationNumber;
    private final StringProperty dateFound;
    private final StringProperty timeFound;
    private final StringProperty luggageType;
    private final StringProperty brand;
    private final StringProperty arrivedWithFlight;
    private final StringProperty luggageTag;
    private final StringProperty locationFound;
    private final StringProperty mainColor;
    private final StringProperty secondColor;
    private final StringProperty size;
    private final StringProperty weight;
    private final StringProperty passengerNameCity;
    private final StringProperty otherCharacteristics;
    private long timeStamp;

    /*
    private Date date;
    private Timestamp stp;
    private int i;
     */
    public FoundLuggage() {
        registrationNumber = new SimpleStringProperty("x");
        dateFound = new SimpleStringProperty("x");
        timeFound = new SimpleStringProperty("x");
        luggageType = new SimpleStringProperty("x");
        brand = new SimpleStringProperty("x");
        arrivedWithFlight = new SimpleStringProperty("x");
        luggageTag = new SimpleStringProperty("x");
        locationFound = new SimpleStringProperty("x");
        mainColor = new SimpleStringProperty("x");
        secondColor = new SimpleStringProperty("x");
        size = new SimpleStringProperty("x");
        weight = new SimpleStringProperty("x");
        passengerNameCity = new SimpleStringProperty("x");
        otherCharacteristics = new SimpleStringProperty("x");
        airport = new SimpleStringProperty("x");
    }

    public FoundLuggage(String rn, String df, String tf, String lt, String b, String awf, String ltag, String lf, String mc, String sc, String s, String w, String psc, String oc) {
        registrationNumber = new SimpleStringProperty(rn);
        dateFound = new SimpleStringProperty(df);
        timeFound = new SimpleStringProperty(tf);
        luggageType = new SimpleStringProperty(lt);
        brand = new SimpleStringProperty(b);
        arrivedWithFlight = new SimpleStringProperty(awf);
        luggageTag = new SimpleStringProperty(ltag);
        locationFound = new SimpleStringProperty(lf);
        mainColor = new SimpleStringProperty(mc);
        secondColor = new SimpleStringProperty(sc);
        size = new SimpleStringProperty(s);
        weight = new SimpleStringProperty(w);
        passengerNameCity = new SimpleStringProperty(psc);
        otherCharacteristics = new SimpleStringProperty(oc);
        airport = new SimpleStringProperty("x");
        /*
        date(dateFound.get());
         */
    }
    
    public FoundLuggage(String rn, String df, String tf, String lt, String b, String awf, String ltag, String lf, String mc, String sc, String s, String w, String psc, String oc, String ar) {
        registrationNumber = new SimpleStringProperty(rn);
        dateFound = new SimpleStringProperty(df);
        timeFound = new SimpleStringProperty(tf);
        luggageType = new SimpleStringProperty(lt);
        brand = new SimpleStringProperty(b);
        arrivedWithFlight = new SimpleStringProperty(awf);
        luggageTag = new SimpleStringProperty(ltag);
        locationFound = new SimpleStringProperty(lf);
        mainColor = new SimpleStringProperty(mc);
        secondColor = new SimpleStringProperty(sc);
        size = new SimpleStringProperty(s);
        weight = new SimpleStringProperty(w);
        passengerNameCity = new SimpleStringProperty(psc);
        otherCharacteristics = new SimpleStringProperty(oc);
        airport = new SimpleStringProperty(ar);
        /*
        date(dateFound.get());
         */
    }

    public void translateIntToSetter(int setterNumber, String value) {

        switch (setterNumber) {
            case 0:
                setRegistrationNumber(value);
                break;
            case 1:
                setDateFound(value);
                break;
            case 2:
                setTimeFound(value);
                break;
            case 3:
                setLuggageType(value);
                break;
            case 4:
                setbrand(value);
                break;
            case 5:
                setArrivedWithFlight(value);
                break;
            case 6:
                setLuggageTag(value);
                break;
            case 7:
                setLocationFound(value);
                break;
            case 8:
                setMainColor(value);
                break;
            case 9:
                setSecondColor(value);
                break;
            case 10:
                setSize(value);
                break;
            case 11:
                setWeight(value);
                break;
            case 12:
                setPassengerNameCity(value);
                break;
            case 13:
                setOtherCharacteristics(value);
                break;
        }
    }

    public String getAirport() {
        return airport.get();
    }

    public void setAirport(String airport) {
        this.airport.set(airport);
    }
    
    public StringProperty airportProperty() {
        return airport;
    }

    public String getRegistrationNumber() {
        return registrationNumber.get();
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber.set(registrationNumber);
    }

    public StringProperty registrationNumberProperty() {
        return registrationNumber;
    }

    public String getDateFound() {
        return dateFound.get();
    }

    public void setDateFound(String dateFound) {
        this.dateFound.set(dateFound);
    }

    public StringProperty dateFoundProperty() {
        return dateFound;
    }

    public String getTimeFound() {
        return timeFound.get();
    }

    public void setTimeFound(String timeFound) {
        this.timeFound.set(timeFound);
    }

    public StringProperty timeFoundProperty() {
        return timeFound;
    }

    public String getLuggageType() {
        return luggageType.get();
    }

    public void setLuggageType(String luggageType) {
        this.luggageType.set(luggageType);
    }

    public StringProperty luggageTypeProperty() {
        return luggageType;
    }

    public String getBrand() {
        return brand.get();
    }

    public void setbrand(String band) {
        this.brand.set(band);
    }

    public StringProperty brandProperty() {
        return brand;
    }

    public String getArrivedWithFlight() {
        return arrivedWithFlight.get();
    }

    public void setArrivedWithFlight(String arrivedWithFlight) {
        this.arrivedWithFlight.set(arrivedWithFlight);
    }

    public StringProperty arrivedWithFlightProperty() {
        return arrivedWithFlight;
    }

    public String getLuggageTag() {
        return luggageTag.get();
    }

    public void setLuggageTag(String luggageTag) {
        this.luggageTag.set(luggageTag);
    }

    public StringProperty luggageTagProperty() {
        return luggageTag;
    }

    public String getLocationFound() {
        return locationFound.get();
    }

    public void setLocationFound(String locationFound) {
        this.locationFound.set(locationFound);
    }

    public StringProperty locationFoundrProperty() {
        return locationFound;
    }

    public String getMainColor() {
        return mainColor.get();
    }

    public void setMainColor(String mainColor) {
        this.mainColor.set(mainColor);
    }

    public StringProperty mainColorProperty() {
        return mainColor;
    }

    public String getSecondColor() {
        return secondColor.get();
    }

    public void setSecondColor(String secondColor) {
        this.secondColor.set(secondColor);
    }

    public StringProperty secondColorProperty() {
        return secondColor;
    }

    public String getSize() {
        return size.get();
    }

    public void setSize(String size) {
        this.size.set(size);
    }

    public StringProperty sizeProperty() {
        return size;
    }

    public String getWeight() {
        return weight.get();
    }

    public void setWeight(String weight) {
        this.weight.set(weight);
    }

    public StringProperty weightProperty() {
        return weight;
    }

    public String getPassengerNameCity() {
        return passengerNameCity.get();
    }

    public void setPassengerNameCity(String passengerNameCity) {
        this.passengerNameCity.set(passengerNameCity);
    }

    public StringProperty passengerNameCityProperty() {
        return passengerNameCity;
    }

    public String getOtherCharacteristics() {
        return otherCharacteristics.get();
    }

    public void setOtherCharacteristics(String otherCharacteristics) {
        this.otherCharacteristics.set(otherCharacteristics);
    }

    public StringProperty otherCharacteristicsProperty() {
        return otherCharacteristics;
    }

    public void checkForUpdate(Database db) throws SQLException {
        List<String> list = new ArrayList();
        Statement sth = db.execute("SELECT registration_number FROM luggage");

        ResultSet rs = sth.getResultSet();
        while (rs.next()) {
            System.out.println(rs.getString("registration_number"));
            /*
            this.user = new User(rs.getString("username"), rs.getString("password"));
            if(user.getName().equals(loginNameInput) && user.getPassword().equals(passwordInput)) {            
                return 1;
            }
             */
        }
    }

    public void deleteFromDatabase(Database db, String tableName) throws SQLException {
        db.execute("DELETE FROM %s WHERE registration_number= '%s'",
                tableName,
                registrationNumber.get());
    }

    public void sendToDatabase(Database db, String table) throws ClassNotFoundException, ParseException, SQLException {
        if (duplicateEntry(db, table)) {
            System.out.println("duplicate entry");
        } else {
            executeQuery(db, table);
        }
    }

    public void sendToDatabaseToSend(Database db, String table) throws ClassNotFoundException, ParseException, SQLException {
        if (duplicateEntry(db, table)) {
            System.out.println("duplicate entry");
        } else {
            Date dNow = new Date();
            long i = dNow.getTime();
            i = i / 1000;
            executeQuery(db, table, String.valueOf(i));
        }
    }

    @Override
    public String toString() {
        return String.format("%-15s %-13s %-10s %-15s %-15s %-22s "
                + "%-15s %-15s %-13s %-17s %-10s %-10s %-34s %s",
                registrationNumber,
                dateFound,
                timeFound,
                luggageType,
                brand,
                arrivedWithFlight,
                luggageTag,
                locationFound,
                mainColor,
                secondColor,
                size,
                weight,
                passengerNameCity,
                otherCharacteristics);
    }

    public String toString2() {

        return String.format("%-15s %-13s %-10s %-15s %-15s %-22s "
                + "%-15s %-15s %-13s %-17s %-10s %-10s %-34s %s",
                registrationNumber.get(),
                dateFound.get(),
                timeFound.get(),
                luggageType.get(),
                brand.get(),
                arrivedWithFlight.get(),
                luggageTag.get(),
                locationFound.get(),
                mainColor.get(),
                secondColor.get(),
                size.get(),
                weight.get(),
                passengerNameCity.get(),
                otherCharacteristics.get());
    }

    public String excelDatetoDate(String str) {
        String[] split = str.split("-");

        switch (split[1].toLowerCase()) {
            case "jan":
                split[1] = "01";                
            case "feb":
                split[1] = "02";
            case "mar":
                split[1] = "03";
            case "apr":
                split[1] = "04";
            case "may":
                split[1] = "05";                        
            case "jun":
                split[1] = "06";
            case "jul":
                split[1] = "07";
            case "aug":
                split[1] = "08";
            case "sep":
                split[1] = "09";
            case "oct":
                split[1] = "10";
            case "nov":
                split[1] = "11";
            case "dec":
                split[1] = "12";
        }

        return String.format("%s-%s-%s", split[0], split[1], split[2]);
    }

    public void dateToTimeStamp(String date, String format) throws ParseException {
        SimpleDateFormat sd = new SimpleDateFormat(format, Locale.ENGLISH);

        //parse string to date
        Date utilDate = sd.parse(date);
        //java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
        //System.out.println(sqlDate);
        //date to long
        long i = utilDate.getTime();
        i = i / 1000;
        timeStamp = i;
    }

    public void executeQuery(Database db, String table) throws ParseException {
        dateToTimeStamp(excelDatetoDate(dateFound.get()), "dd-MM-yyyy");
        String[] databaseStrings = {
            table,
            registrationNumber.get(),
            String.valueOf(timeStamp),
            timeFound.get(),
            luggageType.get(),
            brand.get(),
            arrivedWithFlight.get(),
            luggageTag.get(),
            locationFound.get(),
            mainColor.get(),
            secondColor.get(),
            size.get(),
            weight.get(),
            passengerNameCity.get(),
            otherCharacteristics.get(),
            airport.get()};
        //,'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'
        db.execute("INSERT INTO %s VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                databaseStrings);

    }

    public void executeQuery(Database db, String table, String date) throws ParseException {
        dateToTimeStamp(excelDatetoDate(dateFound.get()), "dd-MM-yyyy");
        String[] databaseStrings = {
            table,
            registrationNumber.get(),
            String.valueOf(timeStamp),
            timeFound.get(),
            luggageType.get(),
            brand.get(),
            arrivedWithFlight.get(),
            luggageTag.get(),
            locationFound.get(),
            mainColor.get(),
            secondColor.get(),
            size.get(),
            weight.get(),
            passengerNameCity.get(),
            otherCharacteristics.get(),
            airport.get(),
            date};
        //,'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'
        db.execute("INSERT INTO %s VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                databaseStrings);

    }

    public Boolean duplicateEntry(Database db, String table) throws SQLException {
        Statement sthSelect = db.execute("SELECT registration_number FROM %s WHERE registration_number = '%s'", table, registrationNumber.get());
        return sthSelect.getResultSet().next();
    }

}
