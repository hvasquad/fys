package nl.kvveenschoten.fys.classes;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;

/**
 * Database wrapper class.
 * 
 * @author Joey Blankendaal (500778751)
 */
public class Database {
    public static final String CONSOLE_PREFIX = "[Database] ";
    
    private static final String DRIVER_CLASS = "com.mysql.cj.jdbc.Driver";
    private static final String DRIVER_PARAMETERS = "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String DRIVER_PREFIX = "jdbc:mysql://";
    
    private Connection connection;
    private String[] settings;
    
    /**
     * Database() constructor method creates a connection our database.
     * 
     * @param host
     * @param username
     * @param password
     * @param databaseName
     */
    public Database(String host, String username, String password, String databaseName) {
        // Save settings for global usage
        this.settings = new String[]{host, username, password, databaseName};
        
        // Check if our desired driver class exists
        try {
            Class.forName(DRIVER_CLASS);
        } catch (ClassNotFoundException e) {
            System.out.println(Database.CONSOLE_PREFIX + "com.mysql.cj.jdbc.Driver ClassNotFoundException");
            
            e.printStackTrace();
        }
        
        Enumeration<Driver> drivers = DriverManager.getDrivers();
        
        // Check if any drivers need to be deregistered to prevent memory leaks
        while (drivers.hasMoreElements()) {
            Driver driver = drivers.nextElement();
            
            if (!driver.getClass().getName().equals(DRIVER_CLASS)) {
                try {
                    DriverManager.deregisterDriver(driver);
                    DriverManager.registerDriver(driver);
                } catch (SQLException e) {
                    System.out.println(Database.CONSOLE_PREFIX + "java.sql.DriverManager SQLException");
                    
                    e.printStackTrace();
                }
            }
        }
        
        // Create a connection with the parameter variables
        try {
            this.connection = DriverManager.getConnection(
                    DRIVER_PREFIX + host + "/" + databaseName + DRIVER_PARAMETERS,
                    username,
                    password
            );
        } catch (SQLException e) {
            System.out.println(Database.CONSOLE_PREFIX + "java.sql.DriverManager.getConnection() SQLException");
            
            e.printStackTrace();
        }
    }
    
    /**
     * close() method closes the connection, if it exists.
     */
    public void close() {
        if(this.connection != null) {
            try {
                this.connection.close();
            } catch(SQLException e) {
                System.out.println(CONSOLE_PREFIX + "java.sql.Connection.close() SQLException");
                
                e.printStackTrace();
            }
        } else {
            System.out.println(CONSOLE_PREFIX + "You're trying to close a connection that's already null.");
        }
    }
    
    /**
     * execute() method performs a statement with an SQL query.
     * 
     * @param query
     * @param params
     * @return 
     */
    public Statement execute(String query, String... params) {
        Statement statement = new Statement(this, query, params);
        
        return statement;
    }
    
    /**
     * getConnection() method returns the connection.
     * 
     * @return 
     */
    public Connection getConnection() {
        return this.connection;
    }
    
    /**
     * getDatabaseName() method returns the database name.
     * 
     * @return 
     */
    public String getDatabaseName() {
        return this.settings[3];
    }
    
    /**
     * getHost() method returns the host.
     * 
     * @return 
     */
    public String getHost() {
        return this.settings[0];
    }
    
    /**
     * getPassword() method returns the password.
     * 
     * @return 
     */
    public String getPassword() {
        return this.settings[2];
    }
    
    /**
     * getUsername() method returns the username.
     * 
     * @return 
     */
    public String getUsername() {
        return this.settings[1];
    }
}