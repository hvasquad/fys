/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.kvveenschoten.fys.classes;


public class ChartObjects {
    
    private long timestamplost;
    private long timestampfound;

    public ChartObjects(long timestamplost, long timestampfound) {
        this.timestamplost = timestamplost;
        this.timestampfound = timestampfound;
    }

    public long getTimestamplost() {
        return timestamplost;
    }

    public long getTimestampfound() {
        return timestampfound;
    }
    
    public int getDifference() {
        return (int)((timestampfound - timestamplost) / 60 / 60 / 24);
    }
    
    
    
    
    
    
}
