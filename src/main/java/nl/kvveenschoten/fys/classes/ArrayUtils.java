package nl.kvveenschoten.fys.classes;

/**
 * ArrayUtils class.
 * 
 * @author Joey Blankendaal (500778751)
 */
public class ArrayUtils {
    /**
     * containsString() method tries to find a specific String in the array.
     * 
     * @param stack
     * @param needle
     * @return 
     */
    public static boolean containsString(String[] stack, String needle) {
        boolean containment = false;
        
        for(String looped : stack) {
            if(containment == false) {
                if(looped.equals(needle)) {
                    containment = true;
                }
            }
        }
        
        return containment;
    }
}
