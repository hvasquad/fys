/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.kvveenschoten.fys.classes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import nl.kvveenschoten.fys.Controllers.MainController;

public class ChartObjectList {

    private List<ChartObjects> list = new ArrayList<>();
    Database db = MainController.db;
    private long datePickerLong;

    public ChartObjectList(long datePickerLong) {
        this.datePickerLong = datePickerLong;
    }

    public ChartObjectList() {
        this.datePickerLong = 0;
    }


    public void differenceOrganizer() throws ParseException, SQLException {
        Statement sth = db.execute("SELECT date_found, date_send FROM luggagetosend ORDER BY date_found");
        ResultSet rs = sth.getResultSet();
        while (rs.next()) {
            list.add(new ChartObjects(rs.getLong("date_found"), rs.getLong("date_send")));
        }

        
    }

    public List<Integer> getDifference() throws ParseException, SQLException {
        List<Integer> arrayList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            arrayList.add(list.get(i).getDifference());
            
        }
        return arrayList;
    }

}
