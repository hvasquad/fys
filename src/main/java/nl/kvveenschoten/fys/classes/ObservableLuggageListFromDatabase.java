/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.kvveenschoten.fys.classes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Daan Oolbekkink
 */
public class ObservableLuggageListFromDatabase {

    private Database db;
    private ObservableList<FoundLuggage> allLuggage;

    public ObservableLuggageListFromDatabase(Database db) {
        this.db = db;
        this.allLuggage = FXCollections.observableArrayList();
    }

    public ObservableList<FoundLuggage> showDatabase(String table) throws SQLException, ClassNotFoundException {
        Statement sth = db.execute(String.format("SELECT * FROM %s ORDER BY registration_number", table));
        ResultSet rs = sth.getResultSet();

        while (rs.next()) {
            allLuggage.add(new FoundLuggage(rs.getString(1), timeStampToStringDate(rs.getString(2)), rs.getString(3),
                    rs.getString(4), rs.getString(5), rs.getString(6),
                    rs.getString(7), rs.getString(8), rs.getString(9),
                    rs.getString(10), rs.getString(11), rs.getString(12),
                    rs.getString(13), rs.getString(14), rs.getString(15)));
        }

        return allLuggage;
    }

    public String timeStampToStringDate(String date) throws SQLException {
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        long i = Long.parseLong(date);
        i = i * 1000;
        //long to date
        Date d = new Date(i);
        return format.format(d);
    }
}
