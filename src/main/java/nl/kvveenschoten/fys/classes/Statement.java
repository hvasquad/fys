package nl.kvveenschoten.fys.classes;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Statement class for the Database wrapper.
 * 
 * @author Joey Blankendaal (500778751)
 */
public class Statement {
    private Database database;
    private String query;
    private String[] params;
    private ResultSet resultSet;
    
    /**
     * Statement() constructor method.
     * 
     * @param database
     * @param query
     * @param params 
     */
    public Statement(Database database, String query, String... params) {
        // Save parameter variables for global usage
        this.database = database;
        this.query = query;
        this.params = params;
        
        java.sql.Statement statement = null;
        
        // Look for all parameters in the SQL query to replace with String variables
        for(String param : this.params) {
            this.query = this.query.replaceFirst("%(.*?)[^'| ]*", param);
        }
        
        // Execute the statement, either through executeUpdate() or executeQuery()
        try {
            statement = database.getConnection().createStatement();
            
            if(this.query.startsWith("INSERT") ||
                    this.query.startsWith("UPDATE") ||
                    this.query.startsWith("DELETE")) {
                statement.executeUpdate(this.query);
            } else {
                this.resultSet = statement.executeQuery(this.query);
            }
        } catch(SQLException e) {
            System.out.println(Database.CONSOLE_PREFIX + "java.sql.Statement.execute() SQLException");
            System.out.println(Database.CONSOLE_PREFIX + "Query: " + this.query);
            
            e.printStackTrace();
        }
    }
    
    /**
     * getQuery() method returns the query.
     * 
     * @return 
     */
    public String getQuery() {
        return this.query;
    }
    
    /**
     * getResultSet() method returns the ResultSet.
     * 
     * @return 
     */
    public ResultSet getResultSet() {
        return this.resultSet;
    }
    
    /**
     * rowCount() method returns the amount of rows.
     * 
     * @return 
     */
    public int rowCount() {
        int count = 0;
        
        // Go through a loop of everything returned from the ResultSet
        try {
            while(this.resultSet.next()) {
                count++;
            }
        } catch(SQLException e) {
            System.out.println(Database.CONSOLE_PREFIX + "java.sql.Statement.rowCount() SQLException");
            
            e.printStackTrace();
        }
        
        return count;
    }
}