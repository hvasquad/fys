package nl.kvveenschoten.fys.classes;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * Mail wrapper class.
 * 
 * @author Joey Blankendaal (500778751)
 */
public class Mail {
    public static final String CONSOLE_PREFIX = "[Mail] ";
    
    private static final String PASSWORD = "wijverdieneneen10";
    private static final String USERNAME = "fys.smtp@gmail.com";
    private static final boolean SMTP_AUTH = true;
    private static final String SMTP_HOST = "smtp.gmail.com";
    private static final boolean SMTP_STARTTLS = true;
    private static final int SMTP_PORT = 587;
    
    private Message message;
    private Multipart multipart;
    
    /**
     * Mail() constructor method creates an email with certain properties.
     * 
     * @param to
     * @param subject
     * @param text 
     */
    public Mail(String to, String subject, String text) {
        // Create all properties from the constant variables
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", String.valueOf(SMTP_AUTH));
        properties.put("mail.smtp.starttls.enable", String.valueOf(SMTP_STARTTLS));
        properties.put("mail.smtp.host", SMTP_HOST);
        properties.put("mail.smtp.port", String.valueOf(SMTP_PORT));
        
        // Login to Gmail with our group account
        Session session = Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(USERNAME, PASSWORD);
            }
        });
        
        this.message = new MimeMessage(session);
        
        // Create all message settings from the paramter variables
        try {
            this.message.setFrom(new InternetAddress(USERNAME));
            this.message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
            this.message.setSubject(subject);
        } catch (AddressException e) {
            System.out.println(CONSOLE_PREFIX + "javax.mail.InternetAddress AddressException");
            
            e.printStackTrace();
        } catch (MessagingException e) {
            System.out.println(CONSOLE_PREFIX + "javax.mail.InternetAddress MessagingException");
            
            e.printStackTrace();
        }
        
        this.multipart = new MimeMultipart();
        
        MimeBodyPart messageBodyPart = new MimeBodyPart();
        
        // Create text message by adding it to the multipart
        try {
            messageBodyPart.setContent(text, "text/html");
            
            this.multipart.addBodyPart(messageBodyPart);
        } catch (MessagingException e) {
            System.out.println(CONSOLE_PREFIX + "javax.mail.MimeBodyPart.setContent() MessagingException");
            
            e.printStackTrace();
        }
    }
    
    /**
     * attachFiles() method adds a file as attachment to the mail.
     * 
     * @param filePaths
     */
    public void attachFiles(String... filePaths) {
        // Loop through every path to attach
        for (String filePath : filePaths) {
            MimeBodyPart attachPart = new MimeBodyPart();
            
            try {
                attachPart.attachFile(filePath);
                
                this.multipart.addBodyPart(attachPart);
            } catch (IOException e) {
                System.out.println(CONSOLE_PREFIX + "javax.mail.Multipart.attachBodyPart() IOException");
                
                e.printStackTrace();
            } catch (MessagingException e) {
                System.out.println(CONSOLE_PREFIX + "javax.mail.Multipart.attachBodyPart() MessagingException");
                
                e.printStackTrace();
            }
       }
    }
    
    /**
     * send() method sends the prepared email.
     */
    public void send() {
        // Set the multipart as the email's content
        try {
            this.message.setContent(this.multipart);
        } catch (MessagingException e) {
            System.out.println(CONSOLE_PREFIX + "javax.mail.Message.setContent() MessagingException");
            
            e.printStackTrace();
        }
        
        // Send the email
        try {
            Transport.send(this.message);
        } catch (MessagingException e) {
            System.out.println(CONSOLE_PREFIX + "javax.mail.Transport.send() MessagingException");
            
            e.printStackTrace();
        }
    }
}