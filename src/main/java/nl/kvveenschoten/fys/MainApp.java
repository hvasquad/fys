package nl.kvveenschoten.fys;

import java.net.URL;
import java.net.URLClassLoader;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import nl.kvveenschoten.fys.Controllers.MainController;
import nl.kvveenschoten.fys.classes.Internationalization;

/**
 * @author Kevin van veenschoten, Joey Blankendaal (500778751)
 * main class for JavaFX application
 * this will the initialize the first "view" and passes needed information to the main controller
**/

public class MainApp extends Application {
    public static ResourceBundle language;
    public static Internationalization internat = new Internationalization("en", "nl");
    
    @Override
    public void start(Stage stage) throws Exception {
        // Set default languages 
        internat.setDefault("en");
        
        // Set the local time
        Locale.setDefault(Locale.ENGLISH);
        
        // Select the correct language
        URL url = getClass().getResource("/languages/");
        ClassLoader classLoader = new URLClassLoader(new URL[]{url});
        MainApp.language = ResourceBundle.getBundle("messages_" + internat.getDefault(), Locale.getDefault(), classLoader);
        
        // Language testing
//        System.out.println("TEST: " + this.language.getString("showPassword"));
        
        // Load the main FXML scene
        FXMLLoader loader = new FXMLLoader(MainController.class.getResource("/fxml/mainScene.fxml"), MainApp.language);
        
        Parent root = loader.load();
        
        Font RobotoFont = Font.loadFont(getClass().getResourceAsStream("/fonts/Roboto-Light.ttf"),14);
        
        // Create a new scene object 
        Scene scene = new Scene(root);
        
        scene.getStylesheets().add("/styles/Styles.css");
        
        // Load the scene (makes the view visable)
        stage.setScene(scene);
                        
        /**
         * gets the controller of the loader object and cast it to a maincontroller object
         * then passes the stage object to the start function for our own form of an initialize 
         */
        MainController controller = (MainController)loader.getController();
        controller.start(stage);
    }

    public static void main(String[] args) {
        launch(args);
    }

}
