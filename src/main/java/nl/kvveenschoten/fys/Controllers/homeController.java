package nl.kvveenschoten.fys.Controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;

/**
 * @author Kevin van veenschoten
 * this controller will contain all the logic for the home page
 * all styles for this page will be in home.css
 */
public class homeController implements Initializable {
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
    
    @FXML
    public void goToExcelAction(MouseEvent event) {
        System.out.println("Excel");
        MainController.loadExcelScene();        
    }
    
    @FXML
    public void goToDamageAction(MouseEvent event) {
        System.out.println("graph1");
        MainController.loadGraph1Scene();
        MainController.loadGraphFooter();
    }
    
    @FXML
    public void goToSearchAction(MouseEvent event) {
        System.out.println("vermissingsformulier");
        MainController.loadVermissingFormulierScene();
        MainController.loadBagageFooter();
    }
}
