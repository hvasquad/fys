package nl.kvveenschoten.fys.Controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import nl.kvveenschoten.fys.MainApp;

/**
 *
 * @author Kevin van veenschoten
 * this controller will contain all the logic for the help page
 * all styles for this page will be in help.css
 */
public class helpController implements Initializable{
    
    @FXML
    private TextArea mainText;
    
    @FXML
    private Text helpTitle;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
       
    }    
    
    public String getHelpString(String helpCase){
        
        StringBuilder helpString = new StringBuilder();
        
        switch(helpCase){
            case "Account aanvragen":
                    helpString.append("1. " + MainApp.language.getString("requestAccount1") + "\n");
                    helpString.append("2. " + MainApp.language.getString("requestAccount2") + ":\n");
                    helpString.append("    " + MainApp.language.getString("requestAccount2info"));
                    helpString.append("3. " + MainApp.language.getString("requestAccount3") + "\n");
                    helpString.append("4. " + MainApp.language.getString("requestAccount4"));
                    break;
            case "Excel file uploaden": 
                    helpString.append("1. " + MainApp.language.getString("uploadExcelFile1") + "\n");
                    helpString.append("2. " + MainApp.language.getString("uploadExcelFile2") + ":\n");
                    helpString.append("   a. " + MainApp.language.getString("uploadExcelFile2a") + "\n");
                    helpString.append("   b. " + MainApp.language.getString("uploadExcelFile2b") + "\n");
                    helpString.append("      " + MainApp.language.getString("uploadExcelFile2b2") + "\n");
                    helpString.append("3. " + MainApp.language.getString("uploadExcelFile3") + "\n");         
                    helpString.append("   " + MainApp.language.getString("uploadExcelFile3twee") + "\n");
                    helpString.append("4. " + MainApp.language.getString("uploadExcelFile4") + "\n");
                    helpString.append("5. " + MainApp.language.getString("uploadExcelFile5")); 
                    break;
            case "Een vermissing aangeven":  
                    helpString.append("1. " + MainApp.language.getString("reportMissing1") + "\n");                                       
                    helpString.append("2. " + MainApp.language.getString("reportMissing2") + "\n");
                    helpString.append("   " + MainApp.language.getString("reportMissing2one") + "\n");
                    helpString.append("3. " + MainApp.language.getString("reportMissing3") + "\n");
                    helpString.append("4. " + MainApp.language.getString("reportMissing4"));
                    break;
            case "Gevonden bagage zoeken":                   
                    helpString.append("1. " + MainApp.language.getString("searchFoundLuggage1") + "\n");
                    helpString.append("2. " + MainApp.language.getString("searchFoundLuggage2") + "\n");
                    helpString.append("3. " + MainApp.language.getString("searchFoundLuggage3") + "\n");
                    helpString.append("4. " + MainApp.language.getString("searchFoundLuggage4") + "\n");
                    helpString.append("5. " + MainApp.language.getString("searchFoundLuggage5") + "\n");
                    helpString.append("6. " + MainApp.language.getString("searchFoundLuggage6") + "\n");
                    helpString.append("7. " + MainApp.language.getString("searchFoundLuggage7"));
                    break;
            case "Te versturen bagage":   
                helpString.append("1. " + MainApp.language.getString("luggageToSend1") + "\n");
                helpString.append("2. " + MainApp.language.getString("luggageToSend2") + "\n"); 
                helpString.append("3. " + MainApp.language.getString("luggageToSend3") + "\n");
                helpString.append("4. " + MainApp.language.getString("luggageToSend4") + "\n");
                helpString.append("5. " + MainApp.language.getString("luggageToSend5") + "\n");
                helpString.append("6. " + MainApp.language.getString("luggageToSend6") + "\n");
                helpString.append("7. " + MainApp.language.getString("luggageToSend7") + "\n");
                helpString.append("8. " + MainApp.language.getString("luggageToSend8") + "\n");
                    break;
            case "Grafieken bekijken":  
                helpString.append("1. " + MainApp.language.getString("viewGraphs1") + "\n");
                helpString.append("2.1 " + MainApp.language.getString("viewGraphs2dot1") + "\n");
                helpString.append("2.2 " + MainApp.language.getString("viewGraphs2dot2") + "\n");
                    break;
                    
                    
        }
        
        return  helpString.toString();
    }
    
    @FXML
    void accountAanvragen(ActionEvent event) {
        mainText.setText(getHelpString("Account aanvragen"));
        helpTitle.setText(MainApp.language.getString("requestAccount"));
    }
    
    @FXML
    void excelUpload(ActionEvent event) {
        mainText.setText(getHelpString("Excel file uploaden"));
        helpTitle.setText(MainApp.language.getString("uploadExcelFile"));
    }

    @FXML
    void gevondenBagage(ActionEvent event) {
        mainText.setText(getHelpString("Een vermissing aangeven"));
        helpTitle.setText(MainApp.language.getString("reportMissing"));
    }

    @FXML
    void grafiekenBekijken(ActionEvent event) {
        mainText.setText(getHelpString("Gevonden bagage zoeken"));
        helpTitle.setText(MainApp.language.getString("searchFoundLuggage"));
    }

    @FXML
    void teVerstruren(ActionEvent event) {
        mainText.setText(getHelpString("Te versturen bagage"));
        helpTitle.setText(MainApp.language.getString("luggageToSend"));
    }

    @FXML
    void vermissingAangeven(ActionEvent event) {
        mainText.setText(getHelpString("Grafieken bekijken"));
        helpTitle.setText(MainApp.language.getString("viewGraphs"));
    }
              
}
