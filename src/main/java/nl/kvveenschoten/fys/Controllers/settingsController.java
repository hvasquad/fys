package nl.kvveenschoten.fys.Controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import nl.kvveenschoten.fys.MainApp;
import nl.kvveenschoten.fys.classes.User;

/**
 * @author Kevin van veenschoten
 * this controller will contain all the logic for the home page
 * all styles for this page will be in settings.css
 */
public class settingsController implements Initializable {
    
    @FXML
    private TextField username;

    @FXML
    private TextField password;

    @FXML
    private TextField type;

    @FXML
    private TextField loginlocation;

    @FXML
    private TextField language;

    @FXML
    private TextField lastLoginSucceslast;

    @FXML
    private TextField lastLoginAttempt;
    
    @FXML
    private TextField newUsername;

    @FXML
    private TextField newPassword;

    @FXML
    private TextField newType;

    @FXML
    private TextField newLanguage;

    @FXML
    private Text createMassage;  
    
    @FXML
    private AnchorPane createUser;
    
    @FXML
    private AnchorPane changeLang;
    
    @FXML
    private ToggleGroup changeLangToggle;
    
    @FXML
    private RadioButton changeLangEn;
    
    @FXML
    private RadioButton changeLangNl;

    /**
     * when all fields of the new user form are filled in it will make a new user
     * 
     * @param event 
     */
    @FXML
    void newUserAction(MouseEvent event) {
        
        System.out.println("checking input.");
        
        if(newUsername.getText().length() > 0 && newPassword.getText().length() > 0 && newType.getText().length() > 0 && newLanguage.getText().length() > 0){
            System.out.println("creating new user.");
            //creating new user in database
            User userCreator = new User(newUsername.getText(), newPassword.getText());                        
            userCreator.createUser(newUsername.getText(), newPassword.getText());
            userCreator.updateOtherUser(newUsername.getText(), newPassword.getText(), newLanguage.getText(), newType.getText()); 
            //clearing input fild
            newUsername.setText("");
            newPassword.setText("");
            newType.setText("");
            newLanguage.setText("");
            //show creation succesfull massage  
            createMassage.setText(MainApp.language.getString("newStaff"));
        }else{
            System.out.println("missing input, please try again.");
            //show creation unsuccesfull massage
            createMassage.setText(MainApp.language.getString("missingInput"));
        }
        
          
    }

    /**
     * loads user credentials in the settings page
     * shows user create form when user is an admin
     * @param location
     * @param resources 
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
                
        User user = MainController.session.getUser();
        
        if(user.isAdmin()){
            createUser.setStyle("-fx-opacity: 100;");
        }else{
            createUser.setStyle("-fx-opacity: 0;");
        }
        
        username.setText(user.getName());
        password.setText(user.getPassword());
        type.setText(user.getType());
        loginlocation.setText(user.getLoginlocation());
        language.setText(user.getLanguage());                
        lastLoginSucceslast.setText(user.getLastLoginSucces());
        lastLoginAttempt.setText(user.getLastLoginAttempt());
     
        // Lang switch
        changeLangToggle = new ToggleGroup();
        
        changeLangEn = new RadioButton("English");
        changeLangEn.setToggleGroup(changeLangToggle);
        changeLangEn.setSelected(true);
        
        changeLangNl = new RadioButton("Dutch");
        changeLangNl.setToggleGroup(changeLangToggle);
        
        changeLangToggle.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> ov, Toggle t, Toggle t1) -> {
            RadioButton chk = (RadioButton) t1.getToggleGroup().getSelectedToggle();
            System.out.println("Selected Radio Button - "+chk.getText());
        });
    }
}
