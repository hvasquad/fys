package nl.kvveenschoten.fys.Controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;;
import javafx.scene.shape.Rectangle;

/**
 * @author Kevin van veenschoten
 * this controller will contain all the logic for the header view
 * all styles for this page will be in header.css
 */
public class headerController implements Initializable {         
    
    @FXML
    public void tempLogOutOnClick(MouseEvent event) {
        System.out.println("logout");
        MainController.loadEmptyHeader();
        MainController.loadLoginScene();
    }

    @FXML
    public void goToHomeAction(MouseEvent event) {        
        System.out.println("home");
        MainController.loadHomeScene();
    }
    
    @FXML
    public void goToSettingsAction(MouseEvent event) {
        System.out.println("settings");
        MainController.loadSettingsScene();
    }
    
    @FXML
    public void goToHelpAction(MouseEvent event) {
        System.out.println("help");
        MainController.loadHelpScene();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
    }
    
}
