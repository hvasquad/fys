package nl.kvveenschoten.fys.Controllers;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Label;
import nl.kvveenschoten.fys.MainApp;
import nl.kvveenschoten.fys.classes.ChartObjectList;
import nl.kvveenschoten.fys.classes.Database;

import nl.kvveenschoten.fys.classes.Statement;

/**
 * This controller contains all logic for the Other Graph page. Style: otherGraph.css
 */
public class Graph1Controller implements Initializable {

//deze werkt nu puur voor de linechart
//om het makkelijk te houden laat hier alleen linechart instaan
    private int sevendays = 0;
    private int threedays = 0;
    private int twenieonedays = 0;
    private int rowcount = 0;
    private long secondDate = 0;

    @FXML
    private PieChart chart;

    @FXML
    private Label label;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        logica();
    }

    public void sorter() throws ParseException, SQLException {
        ChartObjectList chartObjectList;
        if (secondDate == 0) {
            chartObjectList = new ChartObjectList();
        } else {
            chartObjectList = new ChartObjectList(secondDate);
        }
        chartObjectList.differenceOrganizer();
        for (int i = 0; i < chartObjectList.getDifference().size(); i++) {

            if (chartObjectList.getDifference().get(i) <= 3) {
                threedays++;

            } else if (chartObjectList.getDifference().get(i) <= 7) {
                sevendays++;

            } else if (chartObjectList.getDifference().get(i) <= 21) {
                twenieonedays++;

            } else {

            }
        }

    }

    @FXML
    void driedgn(ActionEvent event) throws ParseException, SQLException {
        fillThreeDays();
        label.setText(MainApp.language.getString("threeDays"));
    }

    public void fillThreeDays() {
        ObservableList<PieChart.Data> drie = FXCollections.observableArrayList(
                new PieChart.Data(MainApp.language.getString("repatriated"), threedays),
                new PieChart.Data(MainApp.language.getString("total"), rowcount));

        chart.setData(drie);

        drie.forEach(data
                -> data.nameProperty().bind(
                        Bindings.concat(
                                data.getName(), " ", (int) data.getPieValue(), ""
                        )
                )
        );

       
    }

    @FXML
    void binnen7dagen(ActionEvent event) {
        ObservableList<PieChart.Data> zeven = FXCollections.observableArrayList(
                new PieChart.Data(MainApp.language.getString("repatriated"), sevendays),
                new PieChart.Data(MainApp.language.getString("total"), rowcount));

        chart.setData(zeven);

        zeven.forEach(data
                -> data.nameProperty().bind(
                        Bindings.concat(
                                data.getName(), " ", (int) data.getPieValue(), ""
                        )
                )
        );

        label.setText(MainApp.language.getString("sevenDays"));
    }

    @FXML
    void binnen21dgn(ActionEvent event) {
        ObservableList<PieChart.Data> twintig = FXCollections.observableArrayList(
                new PieChart.Data(MainApp.language.getString("repatriated"), twenieonedays),
                new PieChart.Data(MainApp.language.getString("total"), rowcount));

        chart.setData(twintig);

        twintig.forEach(data
                -> data.nameProperty().bind(
                        Bindings.concat(
                                data.getName(), " ", (int) data.getPieValue(), ""
                        )
                )
        );

        label.setText(MainApp.language.getString("twentyoneDays"));
    }

    public void logica() {

        Database db = MainController.db;
        Statement sth = db.execute("SELECT COUNT(registration_number) FROM luggage WHERE registration_number IS not null;");
        ResultSet rs = sth.getResultSet();
        System.out.println("database connectie");

        try {
            while (rs.next()) {
                rowcount = rs.getInt(1);
            }
        } catch (SQLException ex) {
        }

        try {
            sorter();
        } catch (ParseException | SQLException ex) {
        }
        fillThreeDays();
    }

}
