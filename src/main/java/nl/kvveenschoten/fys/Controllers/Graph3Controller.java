package nl.kvveenschoten.fys.Controllers;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.PieChart;
import nl.kvveenschoten.fys.MainApp;
import nl.kvveenschoten.fys.classes.Database;
import nl.kvveenschoten.fys.classes.Statement;

/**
 * This controller contains all logic for the Sent page. Style: Graph2.css
 *
 * @author Joey Blankendaal
 */
public class Graph3Controller implements Initializable {

    private int luggageToSendCount = 0;
    private int luggageCount = 0;
    private int rowcount = 0;

    @FXML
    private PieChart pieChart;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Database db = MainController.db;
        Statement sth = db.execute("SELECT date_found FROM luggage");
        ResultSet rs = sth.getResultSet();

        try {
            while (rs.next()) {
                rowcount++;
            }
            System.out.println(rowcount);
        } catch (SQLException ex) {

        }

        try {
            neverFoundOrginazer();
        } catch (ParseException | SQLException ex) {
            Logger.getLogger(Graph3Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
        ObservableList<PieChart.Data> drie = FXCollections.observableArrayList(
                new PieChart.Data(MainApp.language.getString("sent"), luggageToSendCount),
                new PieChart.Data(MainApp.language.getString("totalFoundLuggage") + ": ", luggageCount));

        pieChart.setData(drie);
        
        drie.forEach(data ->
                data.nameProperty().bind(
                        Bindings.concat(
                                data.getName(), " ", (int)data.getPieValue(), ""  
                        ) 
                )
        );

    }

    public void neverFoundOrginazer() throws ParseException, SQLException {
        Database db = MainController.db;
        Statement sth = db.execute("SELECT COUNT(registration_number) FROM luggage WHERE registration_number IS not null;");
        ResultSet rs = sth.getResultSet();

        while (rs.next()) {
            luggageCount = rs.getInt(1);

        }

        System.out.println("luggagecount " + luggageCount);

        Statement sth1 = db.execute("SELECT COUNT(registration_number) FROM luggagetosend WHERE registration_number IS not null;");
        ResultSet rs1 = sth1.getResultSet();

        while (rs1.next()) {
            luggageToSendCount = rs1.getInt(1);

        }

        System.out.println("luggagetosend cound " + luggageToSendCount);
    }

}
