package nl.kvveenschoten.fys.Controllers;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.css.PseudoClass;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import nl.kvveenschoten.fys.classes.SessionModule;

/**
 * this controller will handle the login screen actions
 *
 * @author Daan Oolbekkink
 */
public class loginController implements Initializable {

    private PseudoClass falseLogin;

    /**
     * this function will run when the login page gets loaded
     *
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        keyLoginEvent();
        showPassword();
    }

    @FXML
    private Label falseLoginLabel;

    @FXML
    private AnchorPane anchorPaneLogin;

    @FXML
    private TextField loginField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private TextField passwordFieldShow;

    @FXML
    private CheckBox checkBoxLogin;

    /**
     * when u press the login key this function will run
     *
     * @param event
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    @FXML
    public void LoginButtonAction(MouseEvent event) throws SQLException, ClassNotFoundException {
        tryToLogin();
    }

    /**
     * when u press enter on the home page this function will try to log u in
     */
    public void keyLoginEvent() {
        anchorPaneLogin.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent ke) {
                if (ke.getCode().equals(KeyCode.ENTER)) {
                    try {
                        tryToLogin();
                    } catch (SQLException | ClassNotFoundException ex) {
                        Logger.getLogger(loginController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
    }

    /**
     * this function will check if your credentials are legit, when they are u
     * will log in otherwise an error will be appear
     *
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public void tryToLogin() throws SQLException, ClassNotFoundException {
        MainController.session = new SessionModule(loginField.getText(), passwordField.getText());
        if (MainController.session.getSessionState() == 1) {
            System.out.println("Home");

            //load home
            MainController.loadHomeScene();

        } else if (MainController.session.getSessionState() == 0) {
            System.out.println("Login false");
            falseLoginLabel.setVisible(true);
            falseLoginStyleChanges();
        }
    }

    /**
     * this function will handle everything for a false login
     */
    public void falseLoginStyleChanges() {
        falseLogin = PseudoClass.getPseudoClass("error");
        loginField.clear();
        passwordField.clear();
        anchorPaneLogin.requestFocus();
        loginField.pseudoClassStateChanged(falseLogin, true);
        passwordField.pseudoClassStateChanged(falseLogin, true);
        passwordFieldShow.pseudoClassStateChanged(falseLogin, true);
    }

    /**
     * this function will show your password when u press the show password
     * button, when u press it again it will hide your password
     */
    public void showPassword() {
        passwordFieldShow.managedProperty().bind(checkBoxLogin.selectedProperty());
        passwordFieldShow.visibleProperty().bind(checkBoxLogin.selectedProperty());

        passwordField.managedProperty().bind(checkBoxLogin.selectedProperty().not());
        passwordField.visibleProperty().bind(checkBoxLogin.selectedProperty().not());

        // Bind the textField and passwordField text values bidirectionally.
        passwordFieldShow.textProperty().bindBidirectional(passwordField.textProperty());
    }

}
