package nl.kvveenschoten.fys.Controllers;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import nl.kvveenschoten.fys.MainApp;

import nl.kvveenschoten.fys.classes.Database;
import nl.kvveenschoten.fys.classes.Mail;
import nl.kvveenschoten.fys.classes.SessionModule;
import nl.kvveenschoten.fys.classes.Statement;
import nl.kvveenschoten.fys.classes.User;

/**
 * @author kevin van veenschoten this class will be the maincontroller of the
 * javaFx application all the generic method will be in this controller
 */
public class MainController implements Initializable {

    //will be used to bind the stage so we can change it in the maincontroller
    private Stage stage;

    // in our case workspace is the scene every view will be loaded into
    @FXML
    private BorderPane workspace;

    //will be used to bind this class to a variable
    private static MainController instance;

    public static final String DB_HOST = "localhost:3306";
    public static final String DB_USERNAME = "root";
    public static final String DB_PASSWORD = "root";
    public static final String DB_NAME = "fys";
    public static Database db;
    public static SessionModule session;

    /**
     * default initialize
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    /**
     * this is our initialize that gets called from the MainApp.java
     *
     * @param stage is our mainScene in which we show our views
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public void start(Stage stage) throws SQLException, ClassNotFoundException {
        db = new Database(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);

        // Database testing (only works if luggage is already imported)
//        Statement sth = db.execute("SELECT * FROM luggage WHERE type = '%type'", "Bag");
//        
//        System.out.println("Query: " + sth.getQuery());
//        System.out.println("Aantal tassen bij de bagage: " + sth.rowCount());
        
        // Mail testing
//        Mail mail = new Mail(
//               "joeyblankendaal@gmail.com",
//                "Test subject",
//                "Test content, moeten bijlagen bij zitten");
//        mail.attachFiles("src/main/resources/img/CorendonLogo.png");
//        mail.send();
        
        instance = this;
        this.stage = stage;
        loadEmptyHeader();
        loadLoginScene();
        setStageTitle("Corendon Lost-and-Found");
        this.stage.show();

    }

    /**
     * sets the title of the program
     *
     * @param title of the program
     */
    private void setStageTitle(String title) {
        this.stage.setTitle(title);
    }

    /**
     * creates current time
     *
     * @return current time in yyyy-MM-dd format
     */
    public static String createTime() {
        Date dNow = new Date();
        SimpleDateFormat temp = new SimpleDateFormat("yyyy-MM-dd");
        return temp.format(dNow);
    }

    /**
     * transforms an date format to a timestamp format
     *
     * @param date the date
     * @param format the format of the date
     * @return a timestamp
     * @throws ParseException
     */
    public long dateToTimeStamp(String date, String format) throws ParseException {
        SimpleDateFormat sd = new SimpleDateFormat(format, Locale.ENGLISH);

        //parse string to date
        Date utilDate = sd.parse(date);
        //java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
        //System.out.println(sqlDate);

        //date to long
        long i = utilDate.getTime();
        i = i / 1000;
        return i;
    }

    /**
     * this function will load scenes for us
     *
     * @param fxmlFileName is the location of the fxml that we want to load
     * @return when the file is not found return null otherwise return the fxml
     */
    private static Parent loadScene(String fxmlFileName) {
        try {
            return FXMLLoader.load(MainController.class.getResource(fxmlFileName), MainApp.language);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static void loadLoginScene() {
        Parent pane = loadScene("/fxml/loginScene.fxml");
        instance.workspace.setCenter(pane);
    }

    public static void loadHeader() {
        Parent pane = loadScene("/fxml/headerScene.fxml");
        instance.workspace.setTop(pane);
    }

    public static void loadEmptyHeader() {
        Parent pane = loadScene("/fxml/headerCleanScene.fxml");
        instance.workspace.setTop(pane);
    }

    public static void loadHomeScene() {
        loadHeader();
        Parent pane = loadScene("/fxml/homeScene.fxml");
        instance.workspace.setCenter(pane);
    }

    public static void loadSettingsScene() {
        Parent pane = loadScene("/fxml/settingsScene.fxml");
        instance.workspace.setCenter(pane);
    }

    public static void loadHelpScene() {
        Parent pane = loadScene("/fxml/helpScene.fxml");
        instance.workspace.setCenter(pane);
    }

    public static void loadExcelScene() {
        Parent pane = loadScene("/fxml/excelScene.fxml");
        instance.workspace.setCenter(pane);
    }

    public static void loadGraphFooter() {
        Parent pane = loadScene("/fxml/graphFooter.fxml");
        instance.workspace.setBottom(pane);
    }

    public static void loadBagageZoekenScene() {
        Parent pane = loadScene("/fxml/bagagezoekenScene.fxml");
        instance.workspace.setCenter(pane);
    }

    public static void loadGraph3Scene() {
        Parent pane = loadScene("/fxml/Graph3Scene.fxml");
        instance.workspace.setCenter(pane);
    }

    public static void loadGraph1Scene() {
        Parent pane = loadScene("/fxml/Graph1Scene.fxml");
        instance.workspace.setCenter(pane);
    }

    public static void loadBagageFooter() {
        Parent pane = loadScene("/fxml/bagageFooter.fxml");
        instance.workspace.setBottom(pane);
    }

    public static void loadGraph2Scene() {
        Parent pane = loadScene("/fxml/Graph2Scene.fxml");
        instance.workspace.setCenter(pane);
    }

    public static void loadVermissingFormulierScene() {
        Parent pane = loadScene("/fxml/vermissingsformulierScene.fxml");
        instance.workspace.setCenter(pane);
    }

    public static void loadTeVersturenBagageScene() {
        Parent pane = loadScene("/fxml/teversturenbagageScene.fxml");
        instance.workspace.setCenter(pane);
    }
}
