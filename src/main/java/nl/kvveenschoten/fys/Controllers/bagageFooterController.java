package nl.kvveenschoten.fys.Controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Rectangle;

/**
 * @author Kevin van veenschoten this controller will contain all the logic for
 */
public class bagageFooterController implements Initializable {
    
    
    @FXML
    private Rectangle rechts;

    @FXML
    private Rectangle links;

    @FXML
    private Rectangle midden;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        links.setStyle("-fx-opacity: 1");
        midden.setStyle("-fx-opacity: 0.5");
        rechts.setStyle("-fx-opacity: 0.5");
    }

    @FXML
    public void goToTeVersturenBagageAction(MouseEvent event) {
        System.out.println("Loading...");
        links.setStyle("-fx-opacity: 0.5");
        midden.setStyle("-fx-opacity: 0.5");
        rechts.setStyle("-fx-opacity: 1");
        MainController.loadTeVersturenBagageScene();
    }

    @FXML
    public void goToVermissingsFormulierAction(MouseEvent event) {
        System.out.println("Loading...");     
        links.setStyle("-fx-opacity: 1");
        midden.setStyle("-fx-opacity: 0.5");
        rechts.setStyle("-fx-opacity: 0.5");
        MainController.loadVermissingFormulierScene();
    }
        
    @FXML
    public void goToBagageZoekenAction(MouseEvent event) {
        System.out.println("Loading...");        
        links.setStyle("-fx-opacity: 0.5");
        midden.setStyle("-fx-opacity: 1");
        rechts.setStyle("-fx-opacity: 0.5");
        MainController.loadBagageZoekenScene();
    }
}