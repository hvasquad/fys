package nl.kvveenschoten.fys.Controllers;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.DataFormat;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import nl.kvveenschoten.fys.classes.Database;
import nl.kvveenschoten.fys.classes.ExcelReaderModule;

/**
 *
 * @author Kevin van veenschoten this controller will contain all the logic for the excel page all styles for this page will be in excel.css
 */
public class excelController implements Initializable {

    private Database db;

    private String filePath;
    private final FileChooser FILECHOOSER = new FileChooser();
    private ExcelReaderModule excelReader;
    
    @FXML
    private Rectangle popUpBackground;

    @FXML
    private Rectangle popUpLowerRect;

    @FXML
    private Rectangle popUpUpperRect;

    @FXML
    private Rectangle popUpButtonRectangle;

    @FXML
    private Label popUpButtonLabel;

    @FXML
    private ImageView popUpDoneImage;

    @FXML
    private Label popUpexcelFileLabel;

    @FXML
    private Label popUpConfirmLabel;

    @FXML
    private Label popUpLabel;

    @FXML
    private Rectangle uploadRect;

    @FXML
    private ImageView uploadImage;

    @FXML
    private Label uploadLabel;

    @FXML
    private Rectangle rectImport;

    @FXML
    private Label labelImport;

    @FXML
    private TableView foundLuggageTableView;

    @FXML
    private Label fileName;

    @FXML
    private Label lblSheet1;

    @FXML
    private Label lblSheet2;

    @FXML
    private Label lblSheet3;

    @FXML
    private Label lblSheet4;

    @FXML
    private Label lblSheet5;

    @FXML
    private Rectangle btnSheet1;

    @FXML
    private Rectangle btnSheet2;

    @FXML
    private Rectangle btnSheet3;

    @FXML
    private Rectangle btnSheet4;

    @FXML
    private Rectangle btnSheet5;

    @FXML
    public void uploadOnClicked(MouseEvent event) throws IOException, SQLException, ClassNotFoundException, ParseException {
        // Send all the data to the database]
        if (excelReader != null) {
            db = new Database("localhost:3306", "root", "root", "fys");
            excelReader.sendToDatabase(db, "luggage");
            System.out.println("Upload");
            confirmationDialogvisible(true);
        }
    }

    @FXML
    public void confirmationOnClick(MouseEvent event) {
        MainController.loadExcelScene();
        
    }

    @FXML
    public void fileChooserOnClick(MouseEvent event) throws IOException, SQLException, ClassNotFoundException {
        fileChooser();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        dragAndDropFilePath();
        for (int i = 0; i < foundLuggageTableView.getColumns().size(); i++) {
            TableColumn tc = (TableColumn) foundLuggageTableView.getColumns().get(i);
            String propertyName = tc.getId();
            if (propertyName != null && !propertyName.isEmpty()) {
                tc.setCellValueFactory(new PropertyValueFactory<>(propertyName));
            }
        }

    }

    public void setFoundLuggageTableView(int sheetNumber) {
        foundLuggageTableView.setItems(excelReader.getLuggageList(sheetNumber));
    }

    public void previewExcel() throws IOException, SQLException, ClassNotFoundException {

        // read out an excel file
        excelReader = new ExcelReaderModule(filePath);
        excelReader.readExcel();

        // Asks for the number of sheets to know how many buttons it needs to display up to 5 buttons
        int numberOfSheets = excelReader.getNumberOfSheets();
        switch (numberOfSheets) {
            case 1:
                viewButton1(true);
                viewButton2(false);
                viewButton3(false);
                viewButton4(false);
                viewButton5(false);
                break;
            case 2:
                viewButton1(true);
                viewButton2(true);
                viewButton3(false);
                viewButton4(false);
                viewButton5(false);
                break;
            case 3:
                viewButton1(true);
                viewButton2(true);
                viewButton3(true);
                viewButton4(false);
                viewButton5(false);
                break;
            case 4:
                viewButton1(true);
                viewButton2(true);
                viewButton3(true);
                viewButton4(true);
                viewButton5(false);
                break;
            case 5:
                viewButton1(true);
                viewButton2(true);
                viewButton3(true);
                viewButton4(true);
                viewButton5(true);
                break;
            default:
                break;
        }

        // Set the tableview to the show the imported excel content with the default set to 0 aka the first sheet
        setFoundLuggageTableView(0);
        // Make the fileName and TableViewer visible
        fileName.setVisible(true);
        foundLuggageTableView.setVisible(true);
    }

    /*
    Opens the fileChooser, restricts the user to only excel format and returns the filePath
    in the form of a string
     */
    public void fileChooser() throws IOException, SQLException, ClassNotFoundException {
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("MS Excel Document (*.xlsx)", "*.xlsx");
        FILECHOOSER.getExtensionFilters().add(extFilter);
        FILECHOOSER.setTitle("Choose an Excel file");
        File file = FILECHOOSER.showOpenDialog(null);
        if (file != null) {
            filePath = file.getAbsolutePath();
            fileName.setText(file.getName());
            popUpConfirmLabel.setText(file.getName());
            previewExcel();
            makeUploadButtonvisible(true);
        }
    }

    //Handles the drag and drop file import on the excel import page and returns a filePath in the form of a string
    public void dragAndDropFilePath() {
        rectImport.setOnDragOver((DragEvent event) -> {
            Dragboard dragBoard = event.getDragboard();
            if (event.getGestureSource() != rectImport && event.getDragboard().hasFiles()) {
                List<File> phil = dragBoard.getFiles();
                String path = phil.get(0).toPath().toString();
                if (path.endsWith(".xlsx")) {
                    rectImport.setStyle("-fx-stroke: #cc0000");
                    labelImport.setStyle("-fx-text-fill: #cc0000");
                    event.acceptTransferModes(TransferMode.COPY);
                }
            }
            event.consume();
        });

        rectImport.setOnDragDropped((DragEvent event) -> {
            Dragboard dragBoard = event.getDragboard();
            List<File> files = (ArrayList<File>) dragBoard.getContent(DataFormat.FILES);
            boolean succes = false;
            if (files != null) {
                File file = files.get(0);
                filePath = file.getAbsolutePath();
                fileName.setText(file.getName());
                popUpConfirmLabel.setText(file.getName());
                try {
                    previewExcel();
                } catch (IOException | SQLException | ClassNotFoundException ex) {
                    Logger.getLogger(excelController.class.getName()).log(Level.SEVERE, null, ex);
                }

                succes = true;
                makeUploadButtonvisible(succes);
            }
            event.setDropCompleted(succes);
            event.consume();

        });

        rectImport.setOnDragExited((DragEvent event) -> {
            rectImport.setStyle("-fx-stroke: #000000");
            labelImport.setStyle("-fx-text-fill: #000000");
        });

    }

    @FXML
    void onSheet1Click(MouseEvent event) {
        System.out.println("print luggage list please " + 0);
        setFoundLuggageTableView(0);
    }

    @FXML
    void onSheet2Click(MouseEvent event) {
        System.out.println("print luggage list please " + 1);
        setFoundLuggageTableView(1);
    }

    @FXML
    void onSheet3Click(MouseEvent event) {
        System.out.println("print luggage list please " + 2);
        setFoundLuggageTableView(2);
    }

    @FXML
    void onSheet4Click(MouseEvent event) {
        System.out.println("print luggage list please " + 3);
        setFoundLuggageTableView(3);
    }

    @FXML
    void onSheet5Click(MouseEvent event) {
        setFoundLuggageTableView(4);
    }

    public void viewButton1(boolean visible) {
        lblSheet1.setVisible(visible);
        btnSheet1.setVisible(visible);
    }

    public void viewButton2(boolean visible) {
        lblSheet2.setVisible(visible);
        btnSheet2.setVisible(visible);
    }

    public void viewButton3(boolean visible) {
        lblSheet3.setVisible(visible);
        btnSheet3.setVisible(visible);
    }

    public void viewButton4(boolean visible) {
        lblSheet4.setVisible(visible);
        btnSheet4.setVisible(visible);
    }

    public void viewButton5(boolean visible) {
        lblSheet5.setVisible(visible);
        btnSheet5.setVisible(visible);
    }

    public void makeUploadButtonvisible(boolean visible) {
        uploadRect.setVisible(visible);
        uploadImage.setVisible(visible);
        uploadLabel.setVisible(visible);
    }

    public void confirmationDialogvisible(boolean visible) {
        popUpBackground.setVisible(visible);
        popUpLowerRect.setVisible(visible);
        popUpUpperRect.setVisible(visible);
        popUpButtonRectangle.setVisible(visible);
        popUpButtonLabel.setVisible(visible);
        popUpConfirmLabel.setVisible(visible);
        popUpexcelFileLabel.setVisible(visible);
        popUpLabel.setVisible(visible);
        popUpDoneImage.setVisible(visible);
    }

}
