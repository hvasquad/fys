/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.kvveenschoten.fys.Controllers;

import java.net.URL;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ObservableValue;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import nl.kvveenschoten.fys.classes.Database;
import nl.kvveenschoten.fys.classes.FoundLuggage;
import nl.kvveenschoten.fys.classes.LuggageComparator;
import nl.kvveenschoten.fys.classes.ObservableListExtension;
import nl.kvveenschoten.fys.classes.ObservableLuggageListFromDatabase;

/**
 *
 * @author Keegan
 */
public class teversturenbagageController implements Initializable {

    private Database db;
    private ObservableLuggageListFromDatabase OLLFD;
    private ObservableListExtension toSendPage;
    private LuggageComparator lc;

    @FXML
    private AnchorPane AnchorPane;

    @FXML
    private TextField bagageTagField;

    @FXML
    private TextField lastNameField;

    @FXML
    private TextField cityField;

    @FXML
    private TextField arrivedWithFlightField;

    @FXML
    private TextField luggageTypeField;

    @FXML
    private TextField brandField;

    @FXML
    private TextField mainColorField;

    @FXML
    private TextField weightField;

    @FXML
    private TextField sizeField;

    @FXML
    private TextField characteristicField;

    @FXML
    private TextField airportTextField;

    @FXML
    private TableView foundLuggageTableView;

    @FXML
    private TableColumn<FoundLuggage, String> registrationNumber;

    @FXML
    private TableColumn<FoundLuggage, String> dateFound;

    @FXML
    private TableColumn<FoundLuggage, String> timeFound;

    @FXML
    private TableColumn<FoundLuggage, String> luggageType;

    @FXML
    private TableColumn<FoundLuggage, String> brand;

    @FXML
    private TableColumn<FoundLuggage, String> arrivedWithFlight;

    @FXML
    private TableColumn<FoundLuggage, String> luggageTag;

    @FXML
    private TableColumn<FoundLuggage, String> locationFound;

    @FXML
    private TableColumn<FoundLuggage, String> mainColor;

    @FXML
    private TableColumn<FoundLuggage, String> secondColor;

    @FXML
    private TableColumn<FoundLuggage, String> size;

    @FXML
    private TableColumn<FoundLuggage, String> weight;

    @FXML
    private TableColumn<FoundLuggage, String> passengerNameCity;

    @FXML
    private TableColumn<FoundLuggage, String> otherCharacteristics;

    @FXML
    private TableColumn<FoundLuggage, String> airport;

    // Always add initialize first and implements !!
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        db = new Database(MainController.DB_HOST, MainController.DB_USERNAME, MainController.DB_PASSWORD, MainController.DB_NAME);
        checkForCompare();
    }

    @FXML
    public void onRefreshClick(MouseEvent event) {
        checkForCompare();
    }

    @FXML
    void onSendClick(MouseEvent event) throws SQLException, ClassNotFoundException, ParseException {
        if (foundLuggageTableView.getSelectionModel().getSelectedItem() != null) {
            toSendPage.getFoundLuggageList().get(0).sendToDatabaseToSend(db, "luggagetosend");
            toSendPage.getFoundLuggageList().get(0).deleteFromDatabase(db, "luggage_match");
            showTableView();
        }
    }

    public void showTableView() {
        OLLFD = new ObservableLuggageListFromDatabase(db);

        AnchorPane.setFocusTraversable(true);

        // Make every column read the property from its corresponding attribute in FoundLuggage
        registrationNumber.setCellValueFactory(cellData -> cellData.getValue().registrationNumberProperty());
        dateFound.setCellValueFactory(cellData -> cellData.getValue().dateFoundProperty());
        timeFound.setCellValueFactory(cellData -> cellData.getValue().timeFoundProperty());
        luggageType.setCellValueFactory(cellData -> cellData.getValue().luggageTypeProperty());
        brand.setCellValueFactory(cellData -> cellData.getValue().brandProperty());
        arrivedWithFlight.setCellValueFactory(cellData -> cellData.getValue().arrivedWithFlightProperty());
        luggageTag.setCellValueFactory(cellData -> cellData.getValue().luggageTagProperty());
        locationFound.setCellValueFactory(cellData -> cellData.getValue().locationFoundrProperty());
        mainColor.setCellValueFactory(cellData -> cellData.getValue().mainColorProperty());
        secondColor.setCellValueFactory(cellData -> cellData.getValue().secondColorProperty());
        size.setCellValueFactory(cellData -> cellData.getValue().sizeProperty());
        weight.setCellValueFactory(cellData -> cellData.getValue().weightProperty());
        passengerNameCity.setCellValueFactory(cellData -> cellData.getValue().passengerNameCityProperty());
        otherCharacteristics.setCellValueFactory(cellData -> cellData.getValue().otherCharacteristicsProperty());
        airport.setCellValueFactory(cellData -> cellData.getValue().airportProperty());

        // Add an observablelist to the filteredlist
        try {
            FilteredList<FoundLuggage> filteredData = new FilteredList<>(OLLFD.showDatabase("luggage_match"), p -> true);

            filteredData.predicateProperty().bind(Bindings.createObjectBinding(() -> foundluggage -> foundluggage.getLuggageTag().toLowerCase().contains(bagageTagField.getText().toLowerCase())
                    && foundluggage.getPassengerNameCity().toLowerCase().contains(lastNameField.getText().toLowerCase())
                    && foundluggage.getPassengerNameCity().toLowerCase().contains(cityField.getText().toLowerCase())
                    && foundluggage.getArrivedWithFlight().toLowerCase().contains(arrivedWithFlightField.getText().toLowerCase())
                    && foundluggage.getLuggageType().toLowerCase().contains(luggageTypeField.getText().toLowerCase())
                    && foundluggage.getBrand().toLowerCase().contains(brandField.getText().toLowerCase())
                    && foundluggage.getMainColor().toLowerCase().contains(mainColorField.getText().toLowerCase())
                    && foundluggage.getWeight().toLowerCase().contains(weightField.getText().toLowerCase())
                    && foundluggage.getSize().toLowerCase().contains(sizeField.getText().toLowerCase())
                    && foundluggage.getOtherCharacteristics().toLowerCase().contains(characteristicField.getText().toLowerCase())
                    && foundluggage.getAirport().toLowerCase().contains(airportTextField.getText().toLowerCase()),
                    bagageTagField.textProperty(),
                    lastNameField.textProperty(),
                    cityField.textProperty(),
                    arrivedWithFlightField.textProperty(),
                    luggageTypeField.textProperty(),
                    brandField.textProperty(),
                    mainColorField.textProperty(),
                    weightField.textProperty(),
                    sizeField.textProperty(),
                    characteristicField.textProperty(),
                    airportTextField.textProperty()
            ));

            // bind the sorted list to the filteredlist
            SortedList<FoundLuggage> sortedData = new SortedList<>(filteredData);

            // Bind the SortedList comparator to the TableView comparator.
            sortedData.comparatorProperty().bind(foundLuggageTableView.comparatorProperty());

            // Add sorted (and filtered) data to the table.
            foundLuggageTableView.setItems(sortedData);

            toSendPage = new ObservableListExtension();

            foundLuggageTableView.getSelectionModel().selectedItemProperty().addListener((ObservableValue observableValue, Object oldValue, Object newValue) -> {
                //Check whether item is selected and set value of selected item to Label
                if (foundLuggageTableView.getSelectionModel().getSelectedItem() != null) {
                    FoundLuggage selectedLuggage = (FoundLuggage) foundLuggageTableView.getSelectionModel().getSelectedItem();
                    toSendPage.getFoundLuggageList().clear();
                    toSendPage.addToList(selectedLuggage);
                }
            });

        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("getlist error");
        }
    }

    private void checkForCompare() {
        lc = new LuggageComparator(db);
        try {
            lc.CompareLuggageTag();
        } catch (SQLException | ClassNotFoundException | ParseException ex) {
            System.out.println("Geen luggage tag connecties");
        }
        showTableView();
    }

}
