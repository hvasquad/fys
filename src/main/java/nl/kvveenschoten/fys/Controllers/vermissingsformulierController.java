package nl.kvveenschoten.fys.Controllers;

/**
 *
 * @author Keegan 500781475
 */
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import nl.kvveenschoten.fys.MainApp;
import nl.kvveenschoten.fys.classes.Mail;
import nl.kvveenschoten.fys.classes.PDFPrinting;
import nl.kvveenschoten.fys.classes.luggageFormToDatabase;

public class vermissingsformulierController implements Initializable {

    @FXML
    private TextField firstname;

    @FXML
    private TextField surname;

    @FXML
    private TextField flightnumber;

    @FXML
    private TextField cellphone;

    @FXML
    private TextField email;

    @FXML
    private TextField labelnumber;

    @FXML
    private TextField size;

    @FXML
    private TextField location;

    @FXML
    private TextField weight;

    @FXML
    private TextField brand;

    @FXML
    private TextField properties;

    // Omdat we werken met een ChoiceBox waarvan de gegevens waarschijnlijk in een database staan
    // moeten we gebruik maken van een observableList. Eigenlijk alle objecten waar lijsten in staan (grafieken, chars etc.)
    // die makkelijk aanpasbaar moeten zijn.
    // Stap 1: Voeg de twee choiceBoxes toe
    @FXML
    private ChoiceBox colorChoiceBox;

    @FXML
    private ChoiceBox colorChoiceBox2;

    // Stap 2: Voeg een ObservableList toe met alle kleur
    ObservableList<String> kleurenLijst = FXCollections
            .observableArrayList(
                    MainApp.language.getString("yellow"), MainApp.language.getString("olive"), MainApp.language.getString("orange"), MainApp.language.getString("red"),
                    MainApp.language.getString("darkRed"), MainApp.language.getString("pink"), MainApp.language.getString("purple"), MainApp.language.getString("violet"), MainApp.language.getString("blue"),
                    MainApp.language.getString("lightBlue"), MainApp.language.getString("darkBlue"), MainApp.language.getString("cyan"), MainApp.language.getString("green"),
                    MainApp.language.getString("darkGreen"), MainApp.language.getString("lightGreen"), MainApp.language.getString("grey"), MainApp.language.getString("darkGrey"),
                    MainApp.language.getString("lightGrey"), MainApp.language.getString("brown"), MainApp.language.getString("darkBrown"), MainApp.language.getString("lightBrown"), MainApp.language.getString("white"),
                    MainApp.language.getString("black"), MainApp.language.getString("creme"));

    @FXML
    public void handleVerzendFormulier(ActionEvent event) throws IOException {
        System.out.println("PDF-export button clicked...");
        
        // Als de button geklikt wordt dan moet ik de waarde ophalen uit de FXML/Choicebox 
        // Bij een choiseBox moeten de getSelectionModel().getSelectedItem methodes gebruikt worden
        // De output van deze methodes is een object en ik heb een string nodig
        // Ik ga dit object dus casten naar een string (zie syntax hieronder)
        String selectedColor1 = (String) colorChoiceBox.getSelectionModel().getSelectedItem();
        String selectedColor2 = (String) colorChoiceBox2.getSelectionModel().getSelectedItem();
        
        
        
        // This is the function that will print the PDF after the button is clicked!
        PDFPrinting pdfPrint = new PDFPrinting(
                firstname.getText(),
                surname.getText(),
                flightnumber.getText(),
                cellphone.getText(),
                email.getText(),
                labelnumber.getText(),
                selectedColor1,
                selectedColor2,
                size.getText(),
                location.getText(),
                weight.getText(),
                brand.getText(),
                properties.getText());        
        pdfPrint.create();
        
        // Here the automatic mail will be sended
                Mail mail = new Mail(
               email.getText(),
                MainApp.language.getString("yourDigitalForm"),
                getEmail());
        mail.attachFiles("pdf/" + pdfPrint.getDateTimeCreation() + "_corendon_vermissings_formulier.pdf");
        mail.send();
        // Ending of the automated mail

        // Creates a new 'lugage object'
        luggageFormToDatabase luggageDB = new luggageFormToDatabase(firstname.getText(), surname.getText(), flightnumber.getText(), cellphone.getText(), email.getText(), labelnumber.getText(), selectedColor1, selectedColor2, size.getText(), location.getText(), weight.getText(), brand.getText(), properties.getText());
        // Insert information into db
        luggageDB.insertDatabase();
        System.out.println("The following PDF document has been created and filled: corendon_vermissings_formulier.pdf");
        
        System.out.println("Finshed Exporting the PDF-Document.");
    }
    
    public String getFullName(){
        StringBuilder sb = new StringBuilder();
        sb.append(firstname.getText());
        sb.append(" ");
        sb.append(surname.getText());
        sb.append(",");
        
        return sb.toString();
    }
    
    public String getEmail(){
        StringBuilder sb = new java.lang.StringBuilder();
        sb.append(getFullName());
        sb.append("<br>");              
        sb.append("<br>");
        sb.append(MainApp.language.getString("getEmail1") + ": ");
        sb.append(flightnumber.getText());
        sb.append(", ");
        sb.append(MainApp.language.getString("getEmail2"));
        sb.append(System.getProperty("line.separator"));
        sb.append(MainApp.language.getString("getEmail3"));
        sb.append(System.getProperty("line.separator"));
        sb.append(System.getProperty("line.separator"));
        sb.append(MainApp.language.getString("getEmail4"));
        sb.append(System.getProperty("line.separator"));
        sb.append(System.getProperty("<br>"));
        sb.append(MainApp.language.getString("regards"));
        sb.append("<br>");
        sb.append("<br>");
        sb.append("Corendon B.V.");
        
        return sb.toString();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Sets the "normal" value of the first Choicebox
        colorChoiceBox.setValue("Kleur");
        // Fills the Choicebox with items from the list
        colorChoiceBox.setItems(kleurenLijst);

        // This is the second Choicebox/selfmade Colorpicker
        colorChoiceBox2.setValue("Kleur2");
        colorChoiceBox2.setItems(kleurenLijst);
    }
}