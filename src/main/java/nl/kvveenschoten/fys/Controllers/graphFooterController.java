package nl.kvveenschoten.fys.Controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Rectangle;

/**
 * @author Kevin van veenschoten this controller will contain all the logic for
 */
public class graphFooterController implements Initializable {
    
    
    @FXML
    private Rectangle rechts;

    @FXML
    private Rectangle links;

    @FXML
    private Rectangle midden;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        links.setStyle("-fx-opacity: 1");
        //midden.setStyle("-fx-opacity: 0.5");
        rechts.setStyle("-fx-opacity: 0.5");
    }
    
    @FXML
    public void goToGraph3Action(MouseEvent event) {
        //System.out.println("Grap3");
        links.setStyle("-fx-opacity: 0.5");
        //midden.setStyle("-fx-opacity: 0.5");
        rechts.setStyle("-fx-opacity: 1");
        MainController.loadGraph3Scene();
    }
    
    //@FXML
    //public void goToGraph2Action(MouseEvent event) {
    //  System.out.println("graph2");
    //    links.setStyle("-fx-opacity: 0.5");
    //    midden.setStyle("-fx-opacity: 1");
    //    rechts.setStyle("-fx-opacity: 0.5");
    //    MainController.loadGraph2Scene();
    //}
    
    @FXML
    public void goToGraph1Action(MouseEvent event) {
        //System.out.println("graph1");
        links.setStyle("-fx-opacity: 1");
        //midden.setStyle("-fx-opacity: 0.5");
        rechts.setStyle("-fx-opacity: 0.5");
        MainController.loadGraph1Scene();
    }

   
}